#pragma 
#include "City.h"


class CityRegistry
{
public:
	CityRegistry();
	void Init();

	// cluster[lat][lon] holds the id of all cities within that cluster 
	std::vector<std::vector<int>> cluster[180];
	std::vector<City> cities;

private:
	void ReadClusters();
	void ReadCityData();
};