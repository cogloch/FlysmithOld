#include "City.h"


City::City(uint32 id_, uint32 population_,
		   const std::wstring& name_, const std::wstring& country_,
		   float latitude_, float longitude_)
		   : id(id_)
		   , population(population_)
		   , name(name_)
		   , country(country_)
		   , latitude(latitude_)
		   , longitude(longitude_)
{
}

City::City(City&& other)
	: id(std::move(other.id))
	, name(std::move(other.name))
	, country(std::move(other.country))
	, latitude(std::move(other.latitude))
	, longitude(std::move(other.longitude))
	, population(std::move(other.population))
{
}