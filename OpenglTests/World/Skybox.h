#pragma once


class Skybox
{
public:
	void Init();
	void Render(const glm::mat4& viewMatrix, const glm::mat4& projMatrix);

private:
	GLuint m_VAO;
	GLuint m_VBO;
	GLuint m_textureId;
	std::vector<std::string> m_faces;
};