#pragma once
#include "CityRegistry.h"
#include "GeoHelpers.h"


class CityManager
{
public:
	void Init();
	void Update(const SphericalCoords& camPosition);
	void Render();

private:
	CityRegistry m_cityRegistry;

	// Index of city in m_loadedCities the plane(camera) is closest to
	// City simulation is going to take place only in this one
	uint32 m_activeCity;
	std::vector<City> m_loadedCities;
};