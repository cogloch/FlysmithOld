#pragma once


class Terrain
{
public:
	void Render(const glm::vec3& camPos, const glm::mat4& view, const glm::mat4& projection);
	void DrawPatch();
};