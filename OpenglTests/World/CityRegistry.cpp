#include "CityRegistry.h"
#include "Resources\FileManager.h"


CityRegistry::CityRegistry()
{
}

void CityRegistry::Init()
{
	for (auto& meridian : cluster)
		meridian.resize(360);

	ReadClusters();
	ReadCityData();
}

void CityRegistry::ReadClusters()
{
	// Coords are shifted by +90 lat and +180 lon
	std::ifstream file(FileManager::GetDataDirectory() + "clusteredCities.txt");

	int lat = 0,
		lon = 0;

	std::string token;
	while (file >> token)
	{
		if (token[0] == L'#')
		{
			lon++;
			lat += lon / 360;
			lon %= 360;
		}
		else
		{
			cluster[lat][lon].push_back(std::stoi(token));
		}
	}
}

void CityRegistry::ReadCityData()
{
	std::wifstream file(FileManager::GetDataDirectory() + "cityData.txt");

	std::wstring line;
	std::wstring country;
	int currentId = 0;

	while (std::getline(file, line))
	{
		if (line[0] == '~')
			country = line.substr(1);
		else
		{
			auto nameDelim = line.find(' ');
			while (!isdigit(line[nameDelim + 1]))
				nameDelim = line.find(' ', nameDelim + 1);

			auto popDelim = line.find(' ', nameDelim + 1);
			auto coordDelim = line.find(' ', popDelim + 1);
				
			City city(++currentId,
					  std::stoi(line.substr(nameDelim + 1, popDelim - nameDelim)),
					  line.substr(0, nameDelim), 
					  country,
					  std::stof(line.substr(popDelim + 1, coordDelim - popDelim)),
					  std::stof(line.substr(coordDelim + 1))
					  );

			cities.push_back(std::move(city));
		}
	}

	// TODO: Somehow this ends up wrong
	// not a critical issue
	// but wrong
	/*g_logger.Write("Read " + std::to_string(cities.size()) + " cities.");
	g_logger.Write("Cities at 47 x 27: ", false);
	for (auto& city : cluster[137][207])
	{
		std::wstring wName(cities[city].name);
		std::string ascName(wName.begin(), wName.end());
		g_logger.Write(ascName, false);
		g_logger.Write(std::to_string(cities[city].latitude), false);
		g_logger.Write(std::to_string(cities[city].longitude), false);
	}*/
}