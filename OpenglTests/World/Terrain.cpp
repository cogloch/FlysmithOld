#include "Terrain.h"
#include "Rendering\Quad.h"
#include "Rendering\Vertex.h"
#include "Rendering\Shader\Shader.h"


bool bReady = false;

// Standard patch that is offset based on the uniform MVPs
std::vector<Vertex> patchVerts;

void Init()
{
	// Each patch is a quad, 20 x 20 Opengl units
	// A tile is made up of 8 x 8 patches
	// The terrain within the view frustrum is made up of 128x128 instanced quad patches(20x20 Opengl units),
	// initially in a dummy vertex buffer
	// LOD and view frustrum culling are done in the tess control - TODO
	patchVerts.resize(4);
	patchVerts[0].position = { 1.0f, 1.0f, 0.0f };
	patchVerts[1].position = { 1.0f, 0.0f, 0.0f };
	patchVerts[2].position = { 0.0f, 0.0f, 0.0f };
	patchVerts[3].position = { 0.0f, 1.0f, 0.0f };

	glPatchParameteri(GL_PATCH_VERTICES, 4);

	bReady = true;
}

Quad quad;

void Terrain::DrawPatch()
{

	glDrawElements(GL_PATCHES, 4, GL_UNSIGNED_INT, 0);
}

void Terrain::Render(const glm::vec3& camPos, const glm::mat4& view, const glm::mat4& projection)
{
	if (!bReady)
		Init();

	Shader shader("Terrain");

	glBindVertexArray(quad.GetVAO());
	DrawPatch();
}