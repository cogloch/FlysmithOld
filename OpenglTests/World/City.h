#pragma once


class City
{
public:
	City(City&& other);

	City(uint32 id,
		 uint32 population,
		 const std::wstring& name,
		 const std::wstring& country,
		 float latitude,
		 float longitude);

	uint32 id;
	uint32 population;
	std::wstring name;
	std::wstring country;
	float latitude;
	float longitude;
};