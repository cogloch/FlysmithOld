#include "GeoHelpers.h"


SphericalCoords::SphericalCoords()
	: latitute(0.0f)
	, longitude(0.0f)
{
}

SphericalCoords::SphericalCoords(float latitude_, float longitude_)
	: latitute(latitude_)
	, longitude(longitude_)
{
}

glm::vec3 GeoHelpers::SphericalToCartesian(const SphericalCoords& pointDeg)
{
	SphericalCoords point(glm::radians(pointDeg.latitute), glm::radians(pointDeg.longitude));

	return{ EARTH_RADIUS * cosf(point.latitute) * cosf(point.longitude),
			EARTH_RADIUS * cosf(point.latitute) * sinf(point.longitude),
			EARTH_RADIUS * sinf(point.latitute) };
}

SphericalCoords GeoHelpers::CartesianToSpherical(const glm::vec3& point)
{
	return{ glm::degrees(atan2f(point.z, sqrtf(point.x * point.x + point.y * point.y))),
			glm::degrees(atan2f(point.y, point.x)) };
}

SphericalCoords GeoHelpers::GetGeoMidpoint(const std::vector<glm::vec3>& points)
{
	glm::vec3 cartMidpoint;
	for (const auto& point : points)
		cartMidpoint += point;
	cartMidpoint /= points.size();

	return CartesianToSpherical(cartMidpoint);
}

SphericalCoords GeoHelpers::GetGeoMidpoint(const std::vector<SphericalCoords>& sphPoints)
{
	std::vector<glm::vec3> cartPoints(sphPoints.size());

	for (uint32 idx = 0; idx < sphPoints.size(); idx++)
		cartPoints[idx] = SphericalToCartesian(sphPoints[idx]);

	return GetGeoMidpoint(cartPoints);
}