#pragma once


struct SphericalCoords
{
	SphericalCoords();
	SphericalCoords(float, float);

	float latitute;
	float longitude;
};

namespace GeoHelpers
{
	// TODO: Make this some kind of standard value to be followed throughout the game - somehow
	// 1 km = 1 OGL unit
	const float EARTH_RADIUS = 6371.0f;

	// Convert a given point in spherical coordinates(degrees) to its cartesian counterpart
	glm::vec3 SphericalToCartesian(const SphericalCoords&);

	// Convert a given point in cartesian coordinates to its spherical counterpart(degrees)
	SphericalCoords CartesianToSpherical(const glm::vec3&);

	// Calculate the geographical midpoint/geographical center for the given points 
	SphericalCoords GetGeoMidpoint(const std::vector<glm::vec3>&);

	// Calculate the geographical midpoint/geographical center for the given points 
	SphericalCoords GetGeoMidpoint(const std::vector<SphericalCoords>&);
}