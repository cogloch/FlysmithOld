#pragma once

#define DISABLE_RENDERER_MT 1
#define PROF_INIT_WORLD 1


// Opengl
#define GLEW_STATIC
#include <GL\glew.h>
#include <glfw\glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// C++
#include <map>
#include <stack>
#include <queue>
#include <string>
#include <vector>
#include <fstream>
#include <functional>
#include <sstream>

#include <amp.h>
#include <mutex>
#include <thread>
#include <condition_variable>
#include <memory>

// WinAPI
#ifdef _WIN32
#include <Windows.h>
#endif

// Engine
#include "Application\Types.h"
#include "Application\Logger.h"