#include "Tree.h"


template <typename T>
std::hash<std::string> TreeNode<T>::hash;

template <typename T>
TreeNode<T>::TreeNode(const std::string& key_)
	: pParent(nullptr)
	, sKey(key_)
{
	key = hash(key_);
}

template <typename T>
TreeNode<T>::TreeNode(T data_, const std::string& key_)
	: pParent(nullptr)
	, data(data_)
	, sKey(key_)
{
	key = hash(key_);
}

template <typename T>
TreeNode<T>* TreeNode<T>::FindChild(const std::string& key_)
{
	return FindChild(hash(key_));
}

template <typename T>
TreeNode<T>* TreeNode<T>::FindChild(const std::size_t hashedKey)
{
	if (key == hashedKey)
		return this;

	for (auto& child : children)
	{
		auto pFound = child.FindChild(hashedKey);
		if (pFound)
			return pFound;
	}

	return nullptr;
}

template <typename T>
TreeNode<T>* TreeNode<T>::AddChild(const std::string& key_, T data_, const std::string& parent)
{
	TreeNode<T> newNode(data_, key_);

	auto pParent = FindChild(parent);
	if (!pParent)
	{
		newNode.pParent = this;
		children.push_back(newNode);
		return &children[children.size() - 1];
	}
	else
	{
		newNode.pParent = pParent;
		pParent->children.push_back(newNode);
		return &pParent->children[pParent->children.size() - 1];
	}

	return nullptr;
}

template <typename T>
TreeNode<T>* TreeNode<T>::GetRoot()
{
	if (!pParent)
		return this;

	return pParent->GetRoot();
}