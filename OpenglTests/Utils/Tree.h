#pragma once


// TODO: Less retarded implementation
template <typename T>
class TreeNode
{
public:
	TreeNode(const std::string& key = "root");
	TreeNode(T, const std::string& key = "root");

	TreeNode<T>* FindChild(const std::string& key);
	TreeNode<T>* FindChild(const std::size_t hashedKey);
	TreeNode<T>* AddChild(const std::string& key, T, const std::string& parent = "");
	TreeNode<T>* GetRoot();

	T data;
	std::size_t key;
	std::string sKey; // Debug purposes
	TreeNode<T>* pParent;
	std::vector<TreeNode<T>> children;

private:
	static std::hash<std::string> hash;
};

#include "Tree.inl"