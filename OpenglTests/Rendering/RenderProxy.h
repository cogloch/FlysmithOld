#pragma once


// Used for assets that require a camera/view matrix and a perspective projection(ortho views are unnecessary at this point).
// i.e. meshes
struct ModelRenderProxy
{
	uint32    shader;
	uint32    resourceId;
	glm::mat4 transform;

	void Translate(glm::vec3 transVec)
	{
		transform = glm::translate(transform, transVec);
	}
};

// Used for assets that require an orthographic projection matrix, rendered with the same z-value(i.e. no depth testing takes place).
// i.e. mostly UI elements
struct SpriteRenderProxy
{
	// Most times, sprites are simply hidden and not removed, and then recreated.
	bool bVisible;

	// Defaults to a 100x100 square with the top-left corner at (0, 0)
	glm::mat4 transform;
	
	glm::mat4 color;
};