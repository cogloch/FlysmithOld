#pragma once
#include "Vertex.h"


class Quad
{
public:
	Quad();
	static void Init();
	
	bool bDirty;

	void SetSize(const glm::vec2&);
	void SetSize(float, float);
	
	void SetPosition(const glm::vec2&);
	void SetPosition(float, float);
	
	void SetColor(const glm::vec4&);
	void SetRotation(float);

	GLuint GetVAO();

private:
	friend class SpriteRenderer;

	static bool m_bInitialized;

	static GLuint VAO;
	static GLuint VBO;
	static GLuint EBO;

	static std::vector<Vertex> s_vertices;
	static std::vector<GLuint> s_indices;
	
private:
	glm::vec2 m_position;
	glm::vec2 m_size;
	glm::vec4 m_color;
	float	  m_rotation;
};