#pragma once
#include "RenderProxy.h"
#include "Grid.h"

#include "World\Skybox.h"
#include "World\Terrain.h"


// Owned and abused by the render thread. Likewise, the main thread owns a World object.
// On synchronization between the two, data marked as dirty(changed, including creation
// and destruction) is copied over from the main thread's World to the render thread's Scene.
struct Scene
{
	std::vector<ModelRenderProxy> drawables;
	uint32 tempNumVertStat;
	Grid grid;
	
	Skybox skybox;
	Terrain terrain;
};