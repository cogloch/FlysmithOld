#pragma once
#include "Vertex.h"
#include "Shader\ShaderProgram.h"


class Mesh
{
public:
	Mesh();
	Mesh(const std::vector<Vertex>&, const std::vector<GLuint>&, const std::vector<uint32>&);
	void Init();
	void Draw(uint32 shader);

	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<uint32> textures;

private:
	GLuint VAO, VBO, EBO;
};