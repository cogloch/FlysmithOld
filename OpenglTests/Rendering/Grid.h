#pragma once
#include "Shader\Shader.h"


class Grid
{
public:
	Grid();
	void Init();
	void Render(const glm::mat4& viewMatrix, const glm::mat4& projMatrix);

private:
	// Pass vector to rotate the default z axis aligned grid line with
	void DrawAxis(const glm::vec3&, float angle, Shader*);

	float m_spacing;
	float m_lineLength;
	uint32 m_numLines;

	uint32 m_shLocColor;
	uint32 m_shLocModel;
	uint32 m_shLocView;
	uint32 m_shLocProj;

	uint32 m_VAO;
	uint32 m_VBO;
};