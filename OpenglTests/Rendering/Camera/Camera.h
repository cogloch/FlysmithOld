#pragma once
#include "Events\IEventListener.h"
#include "Application\Timer.h"
#include "Application\World.h"


class Camera : public IEventListener
{
public:
	static const glm::vec3 defaultPosition;
	static const glm::vec3 defaultUpVector;

	static const GLfloat defaultYaw;
	static const GLfloat defaultPitch;

	static const GLfloat defaultSpeed;
	static const GLfloat defaultSensitivity;
	static const GLfloat defaultZoom;

public:
	Camera(World* pWorld,
		   glm::vec3 position = defaultPosition,
		   glm::vec3 up = defaultUpVector,
		   GLfloat   yaw = defaultYaw,
		   GLfloat   pitch = defaultPitch);

	virtual ~Camera();
	virtual void HandleEvent(IEvent*, IEvent::EventType) = 0;
	virtual glm::mat4 GetViewMatrix();

	glm::vec3 GetPosition();

protected:
	Timer m_timer;

	// FPSCamera attributes
	glm::vec3 m_position;
	glm::vec3 m_front;
	glm::vec3 m_up;
	glm::vec3 m_right;
	glm::vec3 m_worldUp;

	// Euler angles
	GLfloat m_yaw;
	GLfloat m_pitch;

	// FPSCamera options
	GLfloat m_speed;
	GLfloat m_sensitivity;
	GLfloat m_zoom;

	World* m_pWorld;
};