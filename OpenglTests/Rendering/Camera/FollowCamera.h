#pragma once
#include "Camera.h"
#include "Game\CameraTarget.h"


class FollowCamera : public Camera
{
public:
	FollowCamera(World*);
	void HandleEvent(IEvent*, IEvent::EventType);
	glm::mat4 GetViewMatrix();

	void AttachTarget(std::shared_ptr<CameraTarget>);

private:
	static const float s_minDistToTarget;
	static const float s_maxDistToTarget;
	static const float s_minPitch;

private:
	bool m_invertedControls;
	float m_distToTarget;
	float m_angleAroundTarget;
	std::shared_ptr<CameraTarget> m_pCamTarget;

private:
	void HandleMouseMove(float offsetX, float offsetY);
	void AttachTarget(ArgList);
	void InvertControls(ArgList);
	void Update();
};