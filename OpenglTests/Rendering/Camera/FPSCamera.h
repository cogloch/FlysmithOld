#pragma once
#include "Camera.h"


class FPSCamera : public Camera
{
public:
	FPSCamera(World*);
	void HandleEvent(IEvent*, IEvent::EventType);

private:
	void HandleTick();
	void HandleMouseMove(float offsetX, float offsetY);
	void Update();
};