#include "FPSCamera.h"
#include "Application\InputManager.h"
#include "Events\MouseEv.h"


FPSCamera::FPSCamera(World* pWorld) : Camera(pWorld)
{
	RegisterForEvent(IEvent::MOUSE_MOVE);
	RegisterForEvent(IEvent::TICK);
	Update();
}

void FPSCamera::HandleTick()
{
	if (!g_inputManager.IsMouseBtnDown(MOUSE_RIGHT))
		return;

	m_timer.Update();
	GLfloat velocity = m_speed * m_timer.deltaTime;

	if (g_inputManager.IsKeyDown(GLFW_KEY_W))
		m_position += velocity * m_front;
	if (g_inputManager.IsKeyDown(GLFW_KEY_S))
		m_position -= velocity * m_front;
	if (g_inputManager.IsKeyDown(GLFW_KEY_A))
		m_position -= m_right * velocity;
	if (g_inputManager.IsKeyDown(GLFW_KEY_D))
		m_position += m_right * velocity;

	if (g_inputManager.IsKeyDown(GLFW_KEY_UP))
		m_speed++;
	if (g_inputManager.IsKeyDown(GLFW_KEY_DOWN))
		m_speed--;
}

void FPSCamera::HandleMouseMove(float offsetX, float offsetY)
{
	if (!g_inputManager.IsMouseBtnDown(MOUSE_RIGHT))
		return;

	offsetX *= m_sensitivity;
	offsetY *= m_sensitivity;

	m_yaw += offsetX;
	m_pitch += offsetY;

	static const float minPitch = -89.0f;

	if (m_pitch > 89.0f)
		m_pitch = 89.0f;
	if (m_pitch < minPitch)
		m_pitch = minPitch;

	Update();
}

void FPSCamera::Update()
{
	glm::vec3 front;
	front.x = cos(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));
	front.y = sin(glm::radians(m_pitch));
	front.z = sin(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));
	m_front = glm::normalize(front);

	m_right = glm::normalize(glm::cross(m_front, m_worldUp));
	m_up = glm::normalize(glm::cross(m_right, m_front));
}

void FPSCamera::HandleEvent(IEvent* pEvent, IEvent::EventType evType)
{
	switch (evType)
	{
	case IEvent::MOUSE_MOVE:
	{
		auto pMouseMoveEvent = dynamic_cast<MouseMoveEv*>(pEvent);
		HandleMouseMove(pMouseMoveEvent->x, pMouseMoveEvent->y);
		break;
	}
	case IEvent::TICK:
		HandleTick();
		break;
	}
}