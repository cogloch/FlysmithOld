#include "FollowCamera.h"
#include "Events\MouseEv.h"
#include "Events\CameraEv.h"
#include "Application\InputManager.h"
#include <glm\gtx\rotate_vector.hpp>


const float FollowCamera::s_minDistToTarget = 5.0f;
const float FollowCamera::s_maxDistToTarget = 50.0f;
const float FollowCamera::s_minPitch = -89.0f;


FollowCamera::FollowCamera(World* pWorld)
	: Camera(pWorld)
	, m_distToTarget(10.0f)
	, m_angleAroundTarget(0.0f)
	, m_pCamTarget(std::make_shared<CameraTarget>())
	, m_invertedControls(false)
{
	RegisterForEvent(IEvent::MOUSE_MOVE);
	RegisterForEvent(IEvent::MOUSE_SCROLL);
	RegisterForEvent(IEvent::CAMERA_FOCUS_CHANGE);
	RegisterForEvent(IEvent::TICK);

	RegisterCustomEvent("cam.follow", ConsoleCallback(AttachTarget));
	RegisterCustomEvent("cam.invertControls", ConsoleCallback(InvertControls));

	Update();
}

void FollowCamera::HandleMouseMove(float offsetX, float offsetY)
{
	if (!g_inputManager.IsMouseBtnDown(MOUSE_RIGHT))
		return;

	if (m_invertedControls)
		m_pitch -= offsetY * m_sensitivity;
	else
		m_pitch += offsetY * m_sensitivity;

	if (m_pitch > 89.0f)
		m_pitch = 89.0f;
	if (m_pitch < s_minPitch)
		m_pitch = s_minPitch;

	if (m_invertedControls)
		m_angleAroundTarget -= offsetX * m_sensitivity;
	else
		m_angleAroundTarget += offsetX * m_sensitivity;

	Update();
}

glm::mat4 FollowCamera::GetViewMatrix()
{
	return glm::lookAt(m_position, 
					   glm::vec3(m_pCamTarget->GetTransform() * glm::vec4(1.0f)),
					   glm::vec3(0.0f, 1.0f, 0.0f));
}

void FollowCamera::HandleEvent(IEvent* pEvent, IEvent::EventType evType)
{
	switch (evType)
	{
	case IEvent::MOUSE_MOVE:
	{
		auto pMouseMoveEvent = dynamic_cast<MouseMoveEv*>(pEvent);
		HandleMouseMove(pMouseMoveEvent->x, pMouseMoveEvent->y);
		break;
	}
	case IEvent::MOUSE_SCROLL:
		m_distToTarget -= dynamic_cast<MouseScrollEv*>(pEvent)->y;
		if (m_distToTarget < s_minDistToTarget)
			m_distToTarget = s_minDistToTarget;
		if (m_distToTarget > s_maxDistToTarget)
			m_distToTarget = s_maxDistToTarget;
		Update();
		break;
	case IEvent::CAMERA_FOCUS_CHANGE:
		AttachTarget(dynamic_cast<CameraFocusChangeEv*>(pEvent)->pNewTarget);
		break;
	case IEvent::TICK:
		Update();
		break;
	}
}

void FollowCamera::Update()
{
	auto targetPos = m_pCamTarget->GetTransform() * glm::vec4(1.0f);
	
	float hDistance = m_distToTarget * cos(glm::radians(m_pitch));
	float vDistance = m_distToTarget * sin(glm::radians(m_pitch));

	float offsetX = hDistance * sin(glm::radians(m_angleAroundTarget));
	float offsetZ = hDistance * cos(glm::radians(m_angleAroundTarget));

	m_position.y = targetPos.y + vDistance;
	m_position.x = targetPos.x - offsetX;
	m_position.z = targetPos.z - offsetZ;
}

void FollowCamera::AttachTarget(std::shared_ptr<CameraTarget> pCamTarget)
{
	m_pCamTarget = pCamTarget;
}

void FollowCamera::AttachTarget(ArgList args)
{
	if (args.size() != 1)
		return;

	uint32 entityId = std::stoi(args[0]);
	if (!m_pWorld->HasEntity(entityId))
		return;

	m_pCamTarget = std::make_shared<CameraTarget>();
	m_pWorld->entities[entityId]->AttachComponent(m_pCamTarget);
}

void FollowCamera::InvertControls(ArgList)
{
	/*if (m_invertedControls == 1)
		m_invertedControls = -1;
	else
		m_invertedControls = 1;*/
	m_invertedControls = !m_invertedControls;
}