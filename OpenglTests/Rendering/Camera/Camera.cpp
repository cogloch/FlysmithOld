#include "Camera.h"


const glm::vec3 Camera::defaultPosition = { 0.0f, 20.0f, 15.0f };
const glm::vec3 Camera::defaultUpVector = { 0.0f, 1.0f, 0.0f };
const GLfloat Camera::defaultYaw = -90.0f;
const GLfloat Camera::defaultPitch = -45.0f;
const GLfloat Camera::defaultSpeed = 10.0f;
const GLfloat Camera::defaultSensitivity = 0.25f;
const GLfloat Camera::defaultZoom = 45.0f;


Camera::Camera(World* pWorld, glm::vec3 position, glm::vec3 up, GLfloat yaw, GLfloat pitch) 
	: m_pWorld(pWorld)
	, m_front(0.0f, 0.0f, 0.0f)
	, m_speed(defaultSpeed)
	, m_sensitivity(defaultSensitivity)
	, m_zoom(defaultZoom)
	, m_position(position)
	, m_worldUp(up)
	, m_yaw(yaw)
	, m_pitch(pitch)
{
}

Camera::~Camera()
{
}

glm::mat4 Camera::GetViewMatrix()
{
	return glm::lookAt(m_position, m_position + m_front, m_up);
}

glm::vec3 Camera::GetPosition()
{
	return m_position;
}