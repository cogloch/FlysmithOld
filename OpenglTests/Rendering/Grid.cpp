#include "Grid.h"
#include "Resources\ResourceManager.h"


Grid::Grid() : m_spacing(1.0f), m_lineLength(25.0f), m_numLines(50) {}


void Grid::Init()
{
	glGenVertexArrays(1, &m_VAO);
	glGenBuffers(1, &m_VBO);

	glBindVertexArray(m_VAO);

	float vertices[] = {
		-m_lineLength,
		m_lineLength
	};

	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(float), vertices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, sizeof(float), (GLvoid*)(0));

	glBindVertexArray(0);

	Shader shader("Line");

	m_shLocColor = shader.GetUniformLocation("Color");
	m_shLocModel = shader.GetUniformLocation("model");
	m_shLocView = shader.GetUniformLocation("view");
	m_shLocProj = shader.GetUniformLocation("projection");
}

void Grid::Render(const glm::mat4& viewMatrix, const glm::mat4& projMatrix)
{
	glBindVertexArray(m_VAO);

	Shader shader("Line");
	shader.SetUniform(m_shLocColor, 51.0f / 255.0f, 51.0f / 255.0f, 51.0f / 255.0f, 1.0f);
	shader.SetUniform(m_shLocView, viewMatrix);
	shader.SetUniform(m_shLocProj, projMatrix);

	// Z-axis
	DrawAxis(glm::vec3(0.0f), 0.0f, &shader);
	// X-axis
	DrawAxis(glm::vec3(0.0f, 1.0f, 0.0f), glm::radians(90.0f), &shader);

	glBindVertexArray(0);
}

void Grid::DrawAxis(const glm::vec3& rotation, float angle, Shader* pShader)
{
	glm::mat4 model;

	if (angle)
		model = glm::rotate(model, angle, rotation);

	model = glm::translate(model, glm::vec3(0 - m_numLines / 2 * m_spacing, 0.0f, 0.0f));

	// Along Z
	for (uint32 line = 0; line < m_numLines; line++)
	{
		pShader->SetUniform(m_shLocModel, model);
		glDrawArrays(GL_LINES, 0, 2);

		model = glm::translate(model, glm::vec3(m_spacing, 0.0f, 0.0f));
	}
}


