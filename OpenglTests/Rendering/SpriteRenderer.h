#pragma once


class Quad;

class SpriteRenderer
{
public:
	static void SetProjection(const glm::mat4&);
	static void Render(const Quad&);
	static void Render(const std::vector<Quad>&);

public:
	SpriteRenderer() = delete;
	SpriteRenderer(const SpriteRenderer&) = delete;
	SpriteRenderer& operator=(const SpriteRenderer&) = delete;

private:
	static glm::mat4 s_projection;
};