#include "Model.h"
#include "Resources\FileManager.h"
#include "Resources\ResourceManager.h"
#include <assimp\Importer.hpp>
#include <assimp\postprocess.h>


Model::Model(const std::string& filename) : m_status(NOT_LOADED)
{
	LoadModel(filename);
}

Model::Model() : m_status(NOT_LOADED)
{
}

void Model::LoadModel(const std::string& filename)
{
	//m_status = LOADING;

	Assimp::Importer importer;
	const aiScene* pScene = importer.ReadFile(FileManager::GetModelsDirectory() + filename, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_GenNormals);

	if (!pScene || pScene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !pScene->mRootNode)
	{
		printf("Assimp error: %s\n", importer.GetErrorString());
		return;
	}

	ProcessNode(pScene->mRootNode, pScene);

	//m_status = LOADED;
}

void Model::ProcessNode(aiNode* pNode, const aiScene* pScene)
{
	for (GLuint i = 0; i < pNode->mNumMeshes; i++)
	{
		aiMesh* pMesh = pScene->mMeshes[pNode->mMeshes[i]];
		meshes.push_back(ProcessMesh(pMesh, pScene));
	}

	for (GLuint i = 0; i < pNode->mNumChildren; i++)
	{
		ProcessNode(pNode->mChildren[i], pScene);
	}
}

Mesh Model::ProcessMesh(aiMesh* pAssimpMesh, const aiScene* pScene)
{
	std::vector<Vertex> vertices = GetMeshVertices(pAssimpMesh);
	std::vector<GLuint> indices = GetMeshIndices(pAssimpMesh);
	std::vector<uint32> textures = GetMeshMaterial(pAssimpMesh, pScene);

	return Mesh(std::move(vertices), std::move(indices), std::move(textures));
}

std::vector<Vertex> Model::GetMeshVertices(aiMesh* pMesh)
{
	std::vector<Vertex> vertices;

	for (GLuint i = 0; i < pMesh->mNumVertices; i++)
	{
		Vertex vertex;
		glm::vec3 vector3;

		// Positions
		vector3.x = pMesh->mVertices[i].x;
		vector3.y = pMesh->mVertices[i].y;
		vector3.z = pMesh->mVertices[i].z;
		vertex.position = vector3;

		// Normals
		vector3.x = pMesh->mNormals[i].x;
		vector3.y = pMesh->mNormals[i].y;
		vector3.z = pMesh->mNormals[i].z;
		vertex.normal = vector3;

		// Texture coordinates
		if (pMesh->mTextureCoords[0])
		{
			glm::vec2 vector2;
			vector2.x = pMesh->mTextureCoords[0][i].x;
			vector2.y = pMesh->mTextureCoords[0][i].y;
			vertex.texCoord = vector2;
		}
		else
		{
			vertex.texCoord = glm::vec2(0.0f, 0.0f);
		}

		vertices.push_back(vertex);
	}

	return vertices;
}

std::vector<GLuint> Model::GetMeshIndices(aiMesh* pMesh)
{
	std::vector<GLuint> indices;

	for (GLuint i = 0; i < pMesh->mNumFaces; i++)
	{
		aiFace face = pMesh->mFaces[i];
		for (GLuint j = 0; j < face.mNumIndices; j++)
		{
			indices.push_back(face.mIndices[j]);
		}
	}
	
	return indices;
}

std::vector<uint32> Model::GetMeshMaterial(aiMesh* pMesh, const aiScene* pScene)
{
	std::vector<uint32> textures;

	if (pMesh->mMaterialIndex >= 0)
	{
		aiMaterial* pMaterial = pScene->mMaterials[pMesh->mMaterialIndex];

		std::vector<uint32> diffuseMaps = GetMaterialTextures(pMaterial, aiTextureType_DIFFUSE);
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());

		std::vector<uint32> specularMaps = GetMaterialTextures(pMaterial, aiTextureType_SPECULAR);
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
	}

	return textures;
}

std::vector<uint32> Model::GetMaterialTextures(aiMaterial* pMaterial, aiTextureType type)
{
	std::vector<uint32> textures;

	for (GLuint i = 0; i < pMaterial->GetTextureCount(type); i++)
	{
		aiString path;
		pMaterial->GetTexture(type, i, &path);

		uint32 textureId;
		if (g_resources.LoadTexture2D(path.C_Str(), type, textureId))
			textures.push_back(textureId);
	}
	
	return textures;
}

void Model::Draw(uint32 shader)
{
	/*if (m_status != INITIALIZED)
	{
		if (m_status == LOADED)
			for (auto& mesh : meshes)
			{
				for (auto& mesh : meshes)
					mesh.Init();
				m_status = INITIALIZED;
			}
		else
			return;
	}*/

	for (auto& mesh : meshes)
		mesh.Draw(shader);
}

uint32 Model::GetVertexCount()
{
	/*if (m_status != INITIALIZED)
	{
		if (m_status == LOADED)
		{
			for (auto& mesh : meshes)
				mesh.Init();
			m_status = INITIALIZED;
		}
		else
			return 0;
	}*/

	uint32 numVertices = 0;

	for (auto& mesh : meshes)
		numVertices += mesh.vertices.size();

	return numVertices;
}