#include "Quad.h"


GLuint Quad::VAO = 0;
GLuint Quad::VBO = 0;
GLuint Quad::EBO = 0;
std::vector<Vertex> Quad::s_vertices = {};
std::vector<GLuint> Quad::s_indices = {};
bool Quad::m_bInitialized = false;


Quad::Quad() 
	: m_position(0.0f, 0.0f)
	, m_size(10.0f, 10.0f)
	, m_rotation(0.0f)
	, m_color(1.0f, 1.0f, 1.0f, 1.0f)
{
}

void Quad::Init()
{
	if (m_bInitialized)
		return;

	s_indices.resize(6);
	s_indices = {
		0, 1, 2,
		3, 0, 2
	};

	s_vertices.resize(4);

	s_vertices[0].position = { 1.0f, 1.0f, 0.0f };
	s_vertices[1].position = { 1.0f, 0.0f, 0.0f };
	s_vertices[2].position = { 0.0f, 0.0f, 0.0f };
	s_vertices[3].position = { 0.0f, 1.0f, 0.0f };

	s_vertices[0].texCoord = { 1.0f, 1.0f };
	s_vertices[1].texCoord = { 1.0f, 0.0f };
	s_vertices[2].texCoord = { 0.0f, 0.0f };
	s_vertices[3].texCoord = { 0.0f, 1.0f };

	if (!VAO || !VBO || !EBO)
	{
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glGenBuffers(1, &EBO);
	}

	glBindVertexArray(VAO);

	// ---------------------------- VBO ----------------------------
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, s_vertices.size() * sizeof(Vertex), &s_vertices[0], GL_STATIC_DRAW);
	// -------------------------------------------------------------

	// ---------------------------- EBO ----------------------------
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, s_indices.size() * sizeof(GLuint), &s_indices[0], GL_STATIC_DRAW);
	// -------------------------------------------------------------

	// ---------------------------- VAO ----------------------------
	// Position
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(0));
	// Tex coord
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texCoord));
	// -------------------------------------------------------------

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	m_bInitialized = true;
}

void Quad::SetSize(const glm::vec2& size)
{
	m_size = size;
}

void Quad::SetSize(float width, float height)
{
	m_size.x = width;
	m_size.y = height;
}

void Quad::SetPosition(const glm::vec2& position)
{
	m_position = position;
}

void Quad::SetPosition(float x, float y)
{
	m_position.x = x;
	m_position.y = y;
}

void Quad::SetRotation(float rotation)
{
	m_rotation = rotation;
}

void Quad::SetColor(const glm::vec4& color)
{
	m_color = color;
}

GLuint Quad::GetVAO()
{
	return VAO;
}