#include "Text.h"
#include "Resources\ResourceManager.h"
#include "Application\EngineConfig.h"
#include "Rendering\Shader\Shader.h"


bool TextRenderer::s_bInitialized = false;
GLuint TextRenderer::s_VAO = 0;
GLuint TextRenderer::s_VBO = 0;

uint32 TextRenderer::s_shLocColor = 0;
uint32 TextRenderer::s_shLocProjection = 0;
glm::mat4 TextRenderer::s_shProjection = glm::mat4(1.0f);


void TextRenderer::Init()
{
	if (s_bInitialized)
		return;

	// Vertices
	glGenVertexArrays(1, &s_VAO);
	glGenBuffers(1, &s_VBO);

	glBindVertexArray(s_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, s_VBO);

	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// Shader
	Shader shader("Text");
	s_shLocColor = shader.GetUniformLocation("textColor");
	s_shLocProjection = shader.GetUniformLocation("projection");

	s_shProjection = glm::ortho(0.0f, static_cast<float>(g_config.windowSize.x), 0.0f, static_cast<float>(g_config.windowSize.y));

	s_bInitialized = true;
}

void TextRenderer::Render(const std::string& text, uint32 fontId, glm::vec2 position, GLfloat scale, glm::vec3 color)
{
	position.y = static_cast<float>(g_config.windowSize.y) - position.y;

	Shader shader("Text");
	
	shader.SetUniform(s_shLocProjection, s_shProjection);
	shader.SetUniform(s_shLocColor, color);
	glActiveTexture(GL_TEXTURE0);

	glBindVertexArray(s_VAO);

	Font& font = GetFontById(fontId);

	for (auto& character : text)
	{
		Character ch = font.characters[character];

		GLfloat posX = position.x + ch.bearing.x * scale;
		GLfloat posY = position.y - (ch.size.y - ch.bearing.y) * scale;

		GLfloat w = ch.size.x * scale;
		GLfloat h = ch.size.y * scale;

		// position.x position.y       texCoord.x texCoord.y
		GLfloat vertices[6][4] = {
			// First triangle
			{ posX    , posY + h,    0.0f, 0.0f },
			{ posX    , posY,        0.0f, 1.0f },
			{ posX + w, posY,        1.0f, 1.0f },

			// Second triangle
			{ posX,     posY + h,    0.0f, 0.0f },
			{ posX + w, posY,        1.0f, 1.0f },
			{ posX + w, posY + h,    1.0f, 0.0f }
		};

		glBindTexture(GL_TEXTURE_2D, ch.textureId);
		glBindBuffer(GL_ARRAY_BUFFER, s_VBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		position.x += (ch.advance >> 6) * scale;
	}

	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void TextRenderer::Render(const Text& text)
{
	Render(text.text, text.font, text.position, text.scale, text.color);
}