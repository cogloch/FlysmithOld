#include "Mesh.h"
#include "Resources\ResourceManager.h"


Mesh::Mesh() {}

Mesh::Mesh(const std::vector<Vertex>& vertices_, const std::vector<GLuint>& indices_, const std::vector<uint32>& textures_) :
	vertices(vertices_),
	indices(indices_),
	textures(textures_),
	VAO(0),
	VBO(0),
	EBO(0)
{
	//Init();
}

void Mesh::Draw(uint32 shader)
{
	GLuint diffuseIndex = 1,
		   specularIndex = 1;

	for (GLuint i = 0; i < textures.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i);

		auto texture = g_resources.GetTextureById(textures[i]);

		std::string textureIndex;
		switch (texture.GetTextureType())
		{
		case aiTextureType_DIFFUSE:
			textureIndex = std::to_string(diffuseIndex++);
			break;
		case aiTextureType_SPECULAR:
			textureIndex = std::to_string(specularIndex++);
			break;
		}

		glUniform1f(glGetUniformLocation(shader , ("material." + texture.GetTextureTypeName() + textureIndex).c_str()), i);
		glBindTexture(GL_TEXTURE_2D, texture.GetResource());
	}

	glUniform1f(glGetUniformLocation(shader, "material.shininess"), 16.0f);

	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void Mesh::Init()
{
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);
	
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);

	// Vertex positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(0));

	// Vertex normals
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));

	// Vertex texture coordinates
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texCoord));

	glBindVertexArray(0);
}