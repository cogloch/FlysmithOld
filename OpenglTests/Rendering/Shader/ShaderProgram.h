#pragma once


class ShaderProgram
{
public:
	// Having an (optional) tesselation control shader implies having a tesselation evaluation shader also
	ShaderProgram(const std::string& name, 
				  bool bHasGeometryShader    = false, 
				  bool bHasTessControlShader = false, 
				  bool bHasComputeShader	 = false);

	~ShaderProgram();

	// Non-copyable due to having a handle to an Opengl object which is released on deletion.
	ShaderProgram(const ShaderProgram&) = delete;
	ShaderProgram operator=(const ShaderProgram&) = delete;

	// Use the Shader wrapper instead
	//
	// Bind the program to the pipeline
	//void Use() const;
	// Unbind the program from the pipeline
	//void Release() const;

	// Returns a reference to an Opengl program object(the object's name)
	GLuint GetProgram() const;

	// Returns the unique name the object was created with 
	const std::string& GetName() const;

private:
	void CreateProgram(bool bHasGeometryShader, bool bHasTessControlShader, bool bHasComputeShader);
	
private:
	// Reference to an Opengl shader program
	GLuint m_program;

	// Unique shader name
	std::string m_name;
};