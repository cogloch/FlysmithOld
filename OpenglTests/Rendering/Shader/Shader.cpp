#include "Shader.h"
#include "Resources\ResourceManager.h"


// ----------------------------- Public -----------------------------
Shader::Shader(const ShaderProgram& shaderProgram)
{
	m_oglName = shaderProgram.GetProgram();

#ifdef _DEBUG
	CheckState();
#endif

	glUseProgram(m_oglName);
}

Shader::Shader(const std::string& shaderFilename)
{
	m_oglName = g_resources.GetShaderProgramByName(shaderFilename);

#ifdef _DEBUG
	CheckState();
#endif

	glUseProgram(m_oglName);
}

Shader::Shader(const uint32 oglName)
{
	m_oglName = oglName;

#ifdef _DEBUG
	CheckState();

	if (!glIsProgram(oglName))
		throw "Attempting to bind a shader program with an invalid name.";
#endif

	glUseProgram(m_oglName);
}

void Shader::CheckState()
{
	int32 boundObject = 0;
	glGetIntegerv(GL_CURRENT_PROGRAM, &boundObject);

	if (boundObject)
	{
		auto boundName = g_resources.GetShaderProgramName(boundObject);
		auto currentName = g_resources.GetShaderProgramName(m_oglName);

		throw "Attempting to bind a shader program: " + currentName + " " +
			  "while there was already a bound program: " + boundName;
	}
}

Shader::~Shader()
{
	glUseProgram(0);
}

// ----------------------------- Uniforms -----------------------------
int32 Shader::GetUniformLocation(const std::string& uniformName) const
{
	int32 loc = glGetUniformLocation(m_oglName, uniformName.c_str());
	
#ifdef _DEBUG
	if (loc == -1)
		throw "Invalid uniform name.";
#endif 

	return loc;
}

void Shader::SetUniform(const std::string& name, const float value0) const
{
	glUniform1f(GetUniformLocation(name), value0);
}

void Shader::SetUniform(const std::string& name, const float value0, const float value1) const
{
	glUniform2f(GetUniformLocation(name), value0, value1);
}

void Shader::SetUniform(const std::string& name, const float value0, const float value1, const float value2) const
{
	glUniform3f(GetUniformLocation(name), value0, value1, value2);
}

void Shader::SetUniform(const std::string& name, const float value0, const float value1, const float value2, const float value3) const
{
	glUniform4f(GetUniformLocation(name), value0, value1, value2, value3);
}

void Shader::SetUniform(const std::string& name, const int32 value0) const
{
	glUniform1i(GetUniformLocation(name), value0);
}

void Shader::SetUniform(const std::string& name, const int32 value0, const int32 value1) const
{
	glUniform2i(GetUniformLocation(name), value0, value1);
}

void Shader::SetUniform(const std::string& name, const int32 value0, const int32 value1, const int32 value2) const
{
	glUniform3i(GetUniformLocation(name), value0, value1, value2);
}

void Shader::SetUniform(const std::string& name, const int32 value0, const int32 value1, const int32 value2, const int32 value3) const
{
	glUniform4i(GetUniformLocation(name), value0, value1, value2, value3);
}

void Shader::SetUniform(const std::string& name, const glm::vec2& value) const
{
	glUniform2fv(GetUniformLocation(name), 1, glm::value_ptr(value));
}

void Shader::SetUniform(const std::string& name, const glm::vec3& value) const
{
	glUniform3fv(GetUniformLocation(name), 1, glm::value_ptr(value));
}

void Shader::SetUniform(const std::string& name, const glm::vec4& value) const
{
	glUniform4fv(GetUniformLocation(name), 1, glm::value_ptr(value));
}

void Shader::SetUniform(const std::string& name, const glm::mat2& value, GLboolean transpose) const
{
	glUniformMatrix2fv(GetUniformLocation(name), 1, transpose, glm::value_ptr(value));
}

void Shader::SetUniform(const std::string& name, const glm::mat3& value, GLboolean transpose) const
{
	glUniformMatrix3fv(GetUniformLocation(name), 1, transpose, glm::value_ptr(value));
}

void Shader::SetUniform(const std::string& name, const glm::mat4& value, GLboolean transpose) const
{
	glUniformMatrix4fv(GetUniformLocation(name), 1, transpose, glm::value_ptr(value));
}

void Shader::SetUniform(const int32 loc, const float value0) const
{
	glUniform1f(loc, value0);
}

void Shader::SetUniform(const int32 loc, const float value0, const float value1) const
{
	glUniform2f(loc, value0, value1);
}

void Shader::SetUniform(const int32 loc, const float value0, const float value1, const float value2) const
{
	glUniform3f(loc, value0, value1, value2);
}

void Shader::SetUniform(const int32 loc, const float value0, const float value1, const float value2, const float value3) const
{
	glUniform4f(loc, value0, value1, value2, value3);
}

void Shader::SetUniform(const int32 loc, const int32 value0) const
{
	glUniform1i(loc, value0);
}

void Shader::SetUniform(const int32 loc, const int32 value0, const int32 value1) const
{
	glUniform2i(loc, value0, value1);
}

void Shader::SetUniform(const int32 loc, const int32 value0, const int32 value1, const int32 value2) const
{
	glUniform3i(loc, value0, value1, value2);
}

void Shader::SetUniform(const int32 loc, const int32 value0, const int32 value1, const int32 value2, const int32 value3) const
{
	glUniform4i(loc, value0, value1, value2, value3);
}

void Shader::SetUniform(const int32 loc, const glm::vec2& value) const
{
	glUniform2fv(loc, 1, glm::value_ptr(value));
}

void Shader::SetUniform(const int32 loc, const glm::vec3& value) const
{
	glUniform3fv(loc, 1, glm::value_ptr(value));
}

void Shader::SetUniform(const int32 loc, const glm::vec4& value) const
{
	glUniform4fv(loc, 1, glm::value_ptr(value));
}

void Shader::SetUniform(const int32 loc, const glm::mat2& value, GLboolean transpose) const
{
	glUniformMatrix2fv(loc, 1, transpose, glm::value_ptr(value));
}

void Shader::SetUniform(const int32 loc, const glm::mat3& value, GLboolean transpose) const
{
	glUniformMatrix3fv(loc, 1, transpose, glm::value_ptr(value));
}

void Shader::SetUniform(const int32 loc, const glm::mat4& value, GLboolean transpose) const
{
	glUniformMatrix4fv(loc, 1, transpose, glm::value_ptr(value));
}