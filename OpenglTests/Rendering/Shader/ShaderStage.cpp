#include "ShaderStage.h"
#include "Resources\FileManager.h"


std::map<GLenum, std::string> GLSLExtensions = {
	{ GL_VERTEX_SHADER, ".vert" },			// Vertex shader
	{ GL_FRAGMENT_SHADER, ".frag" },		// Fragment shader
	{ GL_TESS_CONTROL_SHADER, ".tesc" },    // Tesselation control shader
	{ GL_TESS_EVALUATION_SHADER, ".tese" }, // Tesselation evaluation shader
	{ GL_GEOMETRY_SHADER, ".geom" },		// Geometry shader
	{ GL_COMPUTE_SHADER, ".comp" }			// Compute shader
};

// ----------------------------- Public -----------------------------
ShaderStage::ShaderStage(const std::string& name, const GLenum& type, const GLuint& program)
	: m_name(name)
	, m_type(type)
	, m_program(program)
	, m_shader(0)
	, m_source("")
{
	ReadShaderSource(FileManager::GetShaderDirectory() + name + GLSLExtensions[type]);
	CreateShader();
}

ShaderStage::~ShaderStage()
{
	glDetachShader(m_program, m_shader);
	glDeleteShader(m_shader);
}

ShaderStage::ShaderStage(ShaderStage&& other) 
	: m_name(std::move(other.m_name))
	, m_type(std::move(other.m_type))
	, m_program(std::move(other.m_program))
	, m_shader(std::move(other.m_shader))
	, m_source(std::move(other.m_source))
{
}

GLuint ShaderStage::GetShaderObject() const
{
	return m_shader;
}

// ----------------------------- Private -----------------------------
void ShaderStage::ReadShaderSource(const std::string& filename)
{
	std::ifstream file(filename, std::ios::in);
	if (file.fail())
	{
		throw "Can't read file " + filename;
	}

	file.seekg(0, std::ios::end);
	m_source.resize((unsigned int)file.tellg());
	file.seekg(0, std::ios::beg);
	file.read(&m_source[0], m_source.size());
}

void ShaderStage::CreateShader()
{
	m_shader = glCreateShader(m_type);

	const char* shaderCodePtr = m_source.c_str();
	const int shaderCodeSize = m_source.size();

	glShaderSource(m_shader, 1, &shaderCodePtr, &shaderCodeSize);
	glCompileShader(m_shader);

	GLint status = 0;
	glGetShaderiv(m_shader, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		char buffer[512];
		glGetShaderInfoLog(m_shader, 512, nullptr, buffer);
		throw "Error compiling shader: " + m_name + GLSLExtensions[m_type] + "\n" + buffer;
	}
}
