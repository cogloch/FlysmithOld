#pragma once


class ShaderStage
{
public:
	ShaderStage(const std::string& name, const GLenum& type, const GLuint& program);
	~ShaderStage();

	ShaderStage(ShaderStage&&);

	// Having a reference to an Opengl object as member which is released on deletion.
	ShaderStage(const ShaderStage&) = delete;
	ShaderStage& operator=(const ShaderStage&) = delete;
	
	GLuint GetShaderObject() const;

private:
	void ReadShaderSource(const std::string& filename);
	void CreateShader();

private:
	// Name of the shader program this is being linked into
	const std::string m_name;

	// Reference to the program object which this shader is being linked into
	const GLuint m_program;
	
	// May be any of:
	//
	// GL_COMPUTE_SHADER 
	// GL_VERTEX_SHADER 
	// GL_TESS_CONTROL_SHADER 
	// GL_TESS_EVALUATION_SHADER 
	// GL_GEOMETRY_SHADER 
	// GL_FRAGMENT_SHADER
	const GLenum m_type;

	// Reference to a shader object
	GLuint m_shader;

	// Shader source code
	std::string m_source;
};