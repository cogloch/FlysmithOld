#include "ShaderProgram.h"
#include "ShaderStage.h"
#include "Resources\FileManager.h"


// ----------------------------- Public -----------------------------
ShaderProgram::ShaderProgram(const std::string& name, 
							 bool bHasGeometryShader,
							 bool bHasTessControlShader,
							 bool bHasComputeShader)
	: m_name(name)
	, m_program(0)
{
	CreateProgram(bHasGeometryShader, bHasTessControlShader, bHasComputeShader);
}

ShaderProgram::~ShaderProgram() 
{
	glDeleteProgram(m_program);
}

//void ShaderProgram::Use() const
//{
//	glUseProgram(m_program);
//}
//
//void ShaderProgram::Release() const
//{
//	glUseProgram(0);
//}

GLuint ShaderProgram::GetProgram() const
{
	return m_program;
}

const std::string& ShaderProgram::GetName() const
{
	return m_name;
}

// ----------------------------- Private -----------------------------
void ShaderProgram::CreateProgram(bool bHasGeometryShader, bool bHasTessControlShader, bool bHasComputeShader)
{
	m_program = glCreateProgram();

	// Configure the pipelne
	std::vector<ShaderStage> pipeline;
	
	// Avoid calling the destructor prematurely on each move
	// At most 6 stages.
	pipeline.reserve(6);

	// Mandatory stages
	pipeline.emplace_back(m_name, GL_VERTEX_SHADER, m_program);
	pipeline.emplace_back(m_name, GL_FRAGMENT_SHADER, m_program);

	if (bHasGeometryShader)
		pipeline.emplace_back(m_name, GL_GEOMETRY_SHADER, m_program);

	if (bHasComputeShader)
		pipeline.emplace_back(m_name, GL_COMPUTE_SHADER, m_program);

	if (bHasTessControlShader)
	{
		pipeline.emplace_back(m_name, GL_TESS_CONTROL_SHADER, m_program);
		pipeline.emplace_back(m_name, GL_TESS_EVALUATION_SHADER, m_program);
	}

	// Attach shaders and link program
	for (const auto& stage : pipeline)
		glAttachShader(m_program, stage.GetShaderObject());

	glLinkProgram(m_program);

	GLint status = 0;
	glGetProgramiv(m_program, GL_LINK_STATUS, &status);
	if (status == GL_FALSE)
	{
		char buffer[512];
		glGetProgramInfoLog(m_program, 512, nullptr, buffer);
		throw "Shader link error: " + std::string(buffer);
	}
}
