#pragma once
#include "ShaderProgram.h"


// ShaderProgram RAII wrapper
// Usage is weird and ugly, but works.
// TODO: Make usage not weird and ugly.
class Shader
{
public:
	Shader(const ShaderProgram&);

	// Gets the shader program with the passed name from ResourceManager
	Shader(const std::string&);
	
	// Pass an Opengl shader program name
	// Which shouldn't be stored ouside ResourceManager in the first place 
	// Meaning this may go away
	Shader(const uint32);
	~Shader();

	// Use non-cached uniform locations.
	void SetUniform(const std::string&, const float) const;
	void SetUniform(const std::string&, const float, const float) const;
	void SetUniform(const std::string&, const float, const float, const float) const;
	void SetUniform(const std::string&, const float, const float, const float, const float) const;

	void SetUniform(const std::string&, const int32) const;
	void SetUniform(const std::string&, const int32, const int32) const;
	void SetUniform(const std::string&, const int32, const int32, const int32) const;
	void SetUniform(const std::string&, const int32, const int32, const int32, const int32) const;

	void SetUniform(const std::string&, const glm::vec2&) const;
	void SetUniform(const std::string&, const glm::vec3&) const;
	void SetUniform(const std::string&, const glm::vec4&) const;

	void SetUniform(const std::string&, const glm::mat2&, GLboolean transpose = false) const;
	void SetUniform(const std::string&, const glm::mat3&, GLboolean transpose = false) const;
	void SetUniform(const std::string&, const glm::mat4&, GLboolean transpose = false) const;

	// Use cached uniform locations.
	void SetUniform(int32, const float) const;
	void SetUniform(int32, const float, const float) const;
	void SetUniform(int32, const float, const float, const float) const;
	void SetUniform(int32, const float, const float, const float, const float) const;

	void SetUniform(const int32, const int32) const;
	void SetUniform(const int32, const int32, const int32) const;
	void SetUniform(const int32, const int32, const int32, const int32) const;
	void SetUniform(const int32, const int32, const int32, const int32, const int32) const;

	void SetUniform(const int32, const glm::vec2&) const;
	void SetUniform(const int32, const glm::vec3&) const;
	void SetUniform(const int32, const glm::vec4&) const;

	void SetUniform(const int32, const glm::mat2&, GLboolean transpose = false) const;
	void SetUniform(const int32, const glm::mat3&, GLboolean transpose = false) const;
	void SetUniform(const int32, const glm::mat4&, GLboolean transpose = false) const;

	// Used to cache uniform locations.
	int32 GetUniformLocation(const std::string&) const;

public:
	Shader(Shader&&) = delete;
	Shader(const Shader&) = delete;
	Shader& operator = (Shader&&) = delete;
	Shader& operator = (const Shader&) = delete;

private:
	uint32 m_oglName;
	
	// Check if there's another bound shader program object, meaning that someone, somewhere, out there
	// isn't using this. Logs the occurence.
	void CheckState();
};