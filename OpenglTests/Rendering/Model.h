#pragma once
#include "Mesh.h"
#include "Shader\ShaderProgram.h"
#include <assimp\scene.h>


enum Status
{
	NOT_LOADED, // No data loaded from disk
	LOADING,	// A worker is currently reading from disk
	LOADED,		// Data loaded, but not initialized
	INITIALIZED // Ready to render
};

class Model
{
	friend class ResourceManager;
public:
	Model();
	Model(const std::string& filename);
	void LoadModel(const std::string& filename);
	void Draw(uint32 shader);

	uint32 GetVertexCount();

private:
	// Non-copyable
	//Model(const Model&);

	void ProcessNode(aiNode*, const aiScene*);
	Mesh ProcessMesh(aiMesh*, const aiScene*);
	
	std::vector<Vertex> GetMeshVertices(aiMesh*);
	std::vector<GLuint> GetMeshIndices(aiMesh*);
	std::vector<uint32> GetMeshMaterial(aiMesh*, const aiScene*);
	std::vector<uint32> GetMaterialTextures(aiMaterial*, aiTextureType);

	std::vector<Mesh> meshes;

	Status m_status;
};