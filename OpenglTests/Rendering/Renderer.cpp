#include "Renderer.h"
#include "Quad.h"
#include "Text.h"
#include "Model.h"
#include "Events\WindowEv.h"
#include "Application\World.h"
#include "Shader\ShaderProgram.h"
#include "Application\EngineConfig.h"
#include "Resources\ResourceManager.h"


Renderer::Renderer(World* pWorld) 
	: m_pWorld(pWorld)
	, m_camera(pWorld)
{
	RegisterForEvent(IEvent::WINDOW_RESIZE);
}

void Renderer::Init(GLFWwindow* pWindow)
{
	m_pWindow = pWindow;

	float windowWidth = static_cast<float>(g_config.windowSize.x),
		  windowHeight = static_cast<float>(g_config.windowSize.y);
	m_projection = glm::perspective(glm::radians(45.0f), windowWidth / windowHeight, 0.1f, 100.f);

	// 0 - VSYNC off
	// 1 - VSYNC on(60fps)
	// 2 - VSYNC on(30fps)
	glfwSwapInterval(0);
	
	m_scene.grid.Init();
	m_scene.skybox.Init();
	
	m_pUI = std::make_unique<UI>();

	Quad::Init();
	TextRenderer::Init();

	// Start render thread
#if !DISABLE_RENDERER_MT
	glfwMakeContextCurrent(nullptr);
	std::thread renderThread(&Renderer::RenderThread, this);
	renderThread.detach();
#endif
}

uint32 numVerts = 0;

void Renderer::HandleEvent(IEvent* pEvent, IEvent::EventType evType)
{
	switch (evType)
	{
	case IEvent::WINDOW_RESIZE:
		float windowWidth = static_cast<float>(g_config.windowSize.x),
			  windowHeight = static_cast<float>(g_config.windowSize.y);
		m_projection = glm::perspective(glm::radians(45.0f), windowWidth / windowHeight, 0.1f, 100.f);
		break;
	}
}

void Renderer::RenderThread()
{
#if !DISABLE_RENDERER_MT
	glfwMakeContextCurrent(m_pWindow);

	while (!glfwWindowShouldClose(m_pWindow))
	{
		// Wait for the latest simulation data from main thread
		semaphore.dataReady = false;
		std::unique_lock<std::mutex> lock(semaphore.mtx);
		semaphore.condVariable.wait(lock, [this]{
			return semaphore.dataReady;
		});

		STRender();
	}
#else
	STRender();
#endif
}

void Renderer::STRender()
{
	glClearColor(
		85.0f / 255.0f,
		98.0f / 255.0f,
		112.0f / 255.0f,
		1.0f);

	// Move this in a separate visibility culling part(when it will exist)
	uint32 currentNumVerts = 0;
	for (auto& drawable : m_scene.drawables)
		currentNumVerts += g_resources.models[drawable.resourceId].GetVertexCount();

	if (currentNumVerts != numVerts)
	{
		numVerts = currentNumVerts;
		g_logger.Write("Drawing " + std::to_string(numVerts) + " vertices.");
		m_scene.tempNumVertStat = numVerts;
	}
	// -----------------------------------------------------------------------

	// Render scene with the player camera
	//m_pWindow->setView(scene.m_camera);
	//DrawScene();

	// Render the UI layer with the 1:1 default camera
	//m_pWindow->setView(m_pWindow->getDefaultView());
	//DrawUI();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	DrawScene();

	glDisable(GL_DEPTH_TEST);
	DrawUI();
	glEnable(GL_DEPTH_TEST);

	glfwSwapBuffers(m_pWindow);
}

void Renderer::DrawScene()
{
	glm::mat4 view = m_camera.GetViewMatrix();

#ifdef _DEBUG
	try {
#endif

		m_scene.skybox.Render(view, m_projection);
		m_scene.terrain.Render(m_camera.GetPosition(), view, m_projection);

		m_scene.grid.Render(view, m_projection);

		for (auto& drawable : m_scene.drawables)
		{
			Shader shader(drawable.shader);

			shader.SetUniform("model", drawable.transform);
			shader.SetUniform("view", view);
			shader.SetUniform("projection", m_projection);

			g_resources.models[drawable.resourceId].Draw(drawable.shader);
		}

#ifdef _DEBUG
	}
	catch (const std::string& errMsg)
	{
		g_logger.Write(errMsg);
	}
	catch (const char* errMsg)
	{
		g_logger.Write(errMsg);
	}
#endif
}

void Renderer::DrawUI()
{	
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	m_pUI->Render();	
}