#pragma once
#include "glm\glm.hpp"


struct Vertex
{
	glm::vec3 position;
	glm::vec2 texCoord;
	glm::vec3 normal;

	Vertex() {}
	Vertex(const glm::vec3& position_, const glm::vec3& normal_, const glm::vec2& texCoord_) : 
		position(position_),
		normal(normal_),
		texCoord(texCoord_) {}
};