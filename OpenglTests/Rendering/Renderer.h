#pragma once
#include "Scene.h"
#include "Application\World.h"
#include "Camera\FollowCamera.h"
#include "Events\IEventListener.h"
#include "UI\UI.h"


class Renderer : public IEventListener
{
public:
	Renderer(World*);
	void Init(GLFWwindow*);
	void HandleEvent(IEvent*, IEvent::EventType);

	struct Semaphore
	{
		std::mutex mtx;
		std::condition_variable condVariable;
		bool dataReady;
	} semaphore;

	Scene m_scene;
	FollowCamera m_camera;

	void STRender();

private:
	void RenderThread();
	void DrawUI();
	void DrawScene();

	std::unique_ptr<UI> m_pUI;

	GLFWwindow* m_pWindow;
	World*  	m_pWorld;
	Timer	    m_timer;
	glm::mat4   m_projection;
};