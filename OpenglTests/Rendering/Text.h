#pragma once


struct Text
{
	std::string text;
	uint32 font;
	float scale;
	glm::vec2 position;
	glm::vec3 color;
};

class TextRenderer
{
public:
	static void Init();
	static void Render(const Text&);
	static void Render(const std::string& text, uint32 fontId, glm::vec2 position, GLfloat scale, glm::vec3 color);

public:
	TextRenderer() = delete;
	TextRenderer(const TextRenderer&) = delete;
	TextRenderer& operator=(const TextRenderer&) = delete;

private:
	static uint32 s_shLocColor;
	static uint32 s_shLocProjection;
	static glm::mat4 s_shProjection;

	static GLuint s_VAO, s_VBO;
	static bool s_bInitialized;
};