#include "Application\EngineConfig.h"
#include "Shader\Shader.h"
#include "SpriteRenderer.h"
#include "Quad.h"


glm::mat4 SpriteRenderer::s_projection = glm::mat4(1.0f);

void SpriteRenderer::SetProjection(const glm::mat4& projection)
{
	s_projection = projection;
}

void SpriteRenderer::Render(const Quad& quad)
{
	Shader shader("SpriteColor");

	glm::mat4 model;
	glm::vec2 pos = quad.m_position;
	glm::vec2 size = quad.m_size;

	// Translate
	model = glm::translate(model, glm::vec3(pos, 0.0f));

	// Rotate
	model = glm::translate(model, glm::vec3(0.5f * size.x, 0.5f * size.y, 0.0f));
	model = glm::rotate(model, quad.m_rotation, glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::translate(model, glm::vec3(-0.5f * size.x, -0.5f * size.y, 0.0f));

	// Scale
	model = glm::scale(model, glm::vec3(size, 1.0f));

	shader.SetUniform("model", model);
	shader.SetUniform("projection", s_projection);
	shader.SetUniform("inColor", quad.m_color);

	glBindVertexArray(quad.VAO);
	glDrawElements(GL_TRIANGLES, quad.s_indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void SpriteRenderer::Render(const std::vector<Quad>& quads)
{
	for (auto& quad : quads)
		Render(quad);
}
