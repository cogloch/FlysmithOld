#include "Font.h"
#include "FileManager.h"


Character::Character() {}

Character::Character(GLuint textureId_, glm::ivec2 size_, glm::ivec2 bearing_, GLuint advance_) :
	textureId(textureId_),
	size(size_),
	bearing(bearing_),
	advance(advance_)
{
}

Font::Font() {}

void Font::Load(const std::string& fontName, FT_Library& library)
{
	FT_Face face;
	if (FT_New_Face(library, (FileManager::GetFontsDirectory() + fontName).c_str(), 0, &face))
		g_logger.Write("Failed to load font");

	FT_Set_Pixel_Sizes(face, 0, 48);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	for (GLubyte c = 0; c < 128; c++)
	{
		if (FT_Load_Char(face, c, FT_LOAD_RENDER))
		{
			g_logger.Write("Failed to load glyph");
			continue;
		}

		uint32 width = face->glyph->bitmap.width;
		uint32 height = face->glyph->bitmap.rows;
		uint32 bearingX = face->glyph->bitmap_left;
		uint32 bearingY = face->glyph->bitmap_top;

		GLuint texture; 
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);

		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RED,
			width,
			height,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			face->glyph->bitmap.buffer);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		Character character(texture, glm::ivec2(width, height), glm::ivec2(bearingX, bearingY), face->glyph->advance.x);
		characters.insert(std::pair<GLchar, Character>(c, character));
	}
	
	FT_Done_Face(face);
}