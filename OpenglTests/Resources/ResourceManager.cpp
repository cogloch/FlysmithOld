#include "ResourceManager.h"
#include "FileManager.h"

#include "Rendering\Quad.h"


ResourceManager::ResourceManager() {}

ResourceManager::~ResourceManager() 
{
	for (auto& pShaderProg : m_shaders)
	{ 
		delete pShaderProg.second;
		pShaderProg.second = nullptr;
	}
}

void ResourceManager::Init() 
{
	LoadShaders();
	LoadFonts();
	LoadModels();
}

bool ResourceManager::LoadTexture2D(const std::string& filename, aiTextureType type, uint32& outId)
{
	for (auto& texture : textures)
		if (texture.m_path == filename)
			return false;

	Texture2D texture(filename, type);

	textures.push_back(texture);
	outId = textures.size() - 1;

	return true;
}

const Texture2D& ResourceManager::GetTextureById(uint32 id) const
{
	return textures[id];
}

void ResourceManager::LoadFonts()
{
	FT_Library library;
	if (FT_Init_FreeType(&library))
		g_logger.Write("Could not init FreeType library");

	std::string fontNames[] = {
		"Pacifico.ttf",
		"Consolas.ttf"
	};

	for (auto& fontName : fontNames)
	{
		Font font;
		font.Load(fontName, library);
		fonts.push_back(font);
	}

	FT_Done_FreeType(library);
}

void ResourceManager::LoadShaders()
{
	std::ifstream shaderList(FileManager::GetShaderDirectory() + "Shaders.cuc");
	std::string shaderConfig;
	
	enum ShaderStageParam
	{
		GEOMETRY,
		TESS_CONTROL,
		COMPUTE,

		NUM_PARAMS
	};

	const std::map<std::string, ShaderStageParam> stageParamToEnum = {
		{ "geom", GEOMETRY },
		{ "cont", TESS_CONTROL },
		{ "comp", COMPUTE }
	};

	while (std::getline(shaderList, shaderConfig))
	{
		std::stringstream ss(shaderConfig);
		std::istream_iterator<std::string> begin(ss);
		std::istream_iterator<std::string> end;

		std::vector<std::string> shaderParams(begin, end);
		std::string shaderName = shaderParams[0];
		shaderParams = std::vector<std::string>(shaderParams.begin() + 1, shaderParams.end());

		bool bStages[] = {
			false, // Geometry
			false, // Tess control
			false  // Compute
		};

		for (uint32 paramIdx = 0; paramIdx < shaderParams.size(); paramIdx++)
			bStages[stageParamToEnum.at(shaderParams[paramIdx])] = true;

		ShaderProgram* pProgram = nullptr;

		try
		{
			pProgram = new ShaderProgram(shaderName,
										 bStages[GEOMETRY],
										 bStages[TESS_CONTROL],
										 bStages[COMPUTE]);
		}
		catch (const std::string& errorMsg)
		{
			g_logger.Write(errorMsg);
			
			if (pProgram)
			{
				delete pProgram;
				pProgram = nullptr;
			}
		}

		if (pProgram)
			m_shaders[shaderName] = pProgram;
	}
}

const uint32 ResourceManager::GetShaderProgramByName(const std::string& shaderName) const
{
	uint32 shaderProgram = 0;

	auto it = m_shaders.find(shaderName);
	if (it != m_shaders.end())
		shaderProgram = it->second->GetProgram();
	
	return shaderProgram;
}

const std::string& ResourceManager::GetShaderProgramName(int32 targetOglName) const
{
	for (const auto& shaderProgram : m_shaders)
	{
		auto oglName = shaderProgram.second->GetProgram();
		if (oglName == targetOglName)
			return shaderProgram.first;
	}

	return "";
}

void ResourceManager::LoadModels()
{
	std::ifstream modelList(FileManager::GetModelsDirectory() + "Models.cuc");

	std::string modelName;
	std::vector<std::string> modelNames;
	while (std::getline(modelList, modelName)) 
		modelNames.push_back(std::move(modelName));

	models.resize(modelNames.size());

	for (uint32 idx = 0; idx < modelNames.size(); idx++)
	{
		models[idx].LoadModel(modelNames[idx]);
		for (auto& mesh : models[idx].meshes)
			mesh.Init();
	}

	//auto loadThread = [this](int idx, std::string filename) {
	//	models[idx].LoadModel(filename);
	//};

	//std::vector<std::thread> loaders;
	//for (uint32 idx = 0; idx < modelNames.size(); idx++)
	//	loaders.push_back(std::thread(loadThread, idx, modelNames[idx]));
	//	
	//for (auto& thread : loaders)
	//	thread.join();

	//for (auto& model : models)
	//	for (auto& mesh : model.meshes)
	//		mesh.Init();
}

const Model& ResourceManager::GetModel(uint32 id) const
{
	return models[id];
}