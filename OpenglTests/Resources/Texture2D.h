#pragma once
#include "Resources\IResource.h"
#include <assimp\scene.h>


class Texture2D : public IResource
{
	friend class ResourceManager;

public:
	Texture2D(const std::string& filename, aiTextureType);
	aiTextureType GetTextureType();
	std::string   GetTextureTypeName();

private:
	void LoadFromFile(const std::string&);

	std::string   m_path;
	aiTextureType m_type;
	static const std::map<aiTextureType, std::string> textureTypeNames;
};