#include "Texture2D.h"
#include "SOIL\SOIL.h"
#include "FileManager.h"


const std::map<aiTextureType, std::string> Texture2D::textureTypeNames = {
	{ aiTextureType_DIFFUSE, "texture_diffuse" },
	{ aiTextureType_SPECULAR, "texture_specular" }
};

Texture2D::Texture2D(const std::string& filename, aiTextureType type) : m_type(type), m_path(filename)
{
	LoadFromFile(filename);
}

aiTextureType Texture2D::GetTextureType()
{
	return m_type;
}

std::string Texture2D::GetTextureTypeName()
{
	return textureTypeNames.at(m_type);
}

void Texture2D::LoadFromFile(const std::string& filename)
{
	int width, height;
	unsigned char* image = SOIL_load_image((FileManager::GetModelsDirectory() + filename).c_str(), &width, &height, 0, SOIL_LOAD_RGB);

	glGenTextures(1, &m_resource);
	glBindTexture(GL_TEXTURE_2D, m_resource);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);

	SOIL_free_image_data(image);
	glBindTexture(GL_TEXTURE_2D, 0);
}