#pragma once
#include "Font.h"
#include "Texture2D.h"
#include "Rendering\Model.h"
#include "Rendering\Shader\ShaderProgram.h"


#define GetFontById(fontId) g_resources.fonts[(fontId)]

class ResourceManager
{
public:
	ResourceManager();
	~ResourceManager();

	void Init();

	bool LoadTexture2D(const std::string& filename, aiTextureType, uint32& outId);
	const Texture2D& GetTextureById(uint32) const;

	std::vector<Font>	   fonts;
	std::vector<Model>	   models;
	std::vector<Texture2D> textures;

	const Model& GetModel(uint32 id) const;

	// Returns 0 on failure
	const uint32 GetShaderProgramByName(const std::string&) const;
	
	// Get a (human-readable) name of the shader program with the passed Ogl object name
	// e.g. A shader called "rainbows" was linked into a program with a generated Ogl name == 5
	// GetShaderProgramName(5) returns "rainbows"
	const std::string& GetShaderProgramName(int32) const;

private:
	void LoadFonts();
	void LoadShaders();
	void LoadModels();

private:
	// TODO: use integer keys
	std::map<std::string, ShaderProgram*> m_shaders;	
};

extern ResourceManager g_resources;