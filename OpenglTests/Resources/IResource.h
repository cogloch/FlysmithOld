#pragma once


class IResource
{
public:
	virtual void LoadFromFile(const std::string&) = 0;
	GLuint GetResource();

protected:
	enum class State
	{
		NOT_LOADED,
		LOADING,
		LOADED
	} m_state;

	GLuint m_resource;
};