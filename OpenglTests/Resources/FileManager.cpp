#include "FileManager.h"

std::string FileManager::s_exeDirectory;
std::string FileManager::s_dataDirectory;
std::string FileManager::s_fontsDirectory;
std::string FileManager::s_modelsDirectory;
std::string FileManager::s_shaderDirectory;
std::string FileManager::s_scenarioDirectory;


void FileManager::Init()
{
	char buffer[MAX_PATH];
	GetModuleFileNameA(NULL, buffer, MAX_PATH);

	s_exeDirectory = buffer;
	s_exeDirectory = s_exeDirectory.substr(0, s_exeDirectory.find_last_of("\\") + 1);

	s_dataDirectory     = s_exeDirectory + R"(Data\)";
	s_fontsDirectory    = s_exeDirectory + R"(Assets\Fonts\)";
	s_modelsDirectory   = s_exeDirectory + R"(Assets\)";
	s_shaderDirectory   = s_exeDirectory + R"(Shaders\)";
	s_scenarioDirectory = s_exeDirectory + R"(Scenarios\)";
}

const std::string&  FileManager::GetExeDirectory()
{
	return s_exeDirectory;
}

const std::string&  FileManager::GetModelsDirectory()
{
	return s_modelsDirectory;
}

const std::string&  FileManager::GetShaderDirectory()
{
	return s_shaderDirectory;
}

const std::string&  FileManager::GetFontsDirectory()
{
	return s_fontsDirectory;
}

const std::string&  FileManager::GetDataDirectory()
{
	return s_dataDirectory;
}

const std::string& FileManager::GetScenarioDirectory()
{
	return s_scenarioDirectory;
}

std::vector<std::string> FileManager::GetFilesInDirectory(const std::string& directory_)
{
	std::string directory = directory_;
	//if (directory[directory.size() - 1] == '\\')
	//	directory.pop_back();
	directory += '*';

	std::vector<std::string> files;

	WIN32_FIND_DATA findFileData;
	HANDLE hFind = FindFirstFile(directory.c_str(), &findFileData);
	if (hFind == INVALID_HANDLE_VALUE)
		return files;

	// TODO: Figure why it finds two files named "." and ".."
	do
	{
		files.push_back(findFileData.cFileName);
	} while (FindNextFile(hFind, &findFileData) != 0);

	FindClose(hFind);

	return files;
}