#pragma once


class FileManager
{
public:
	static const std::string& GetExeDirectory();
	static const std::string& GetDataDirectory();
	static const std::string& GetFontsDirectory();
	static const std::string& GetModelsDirectory();
	static const std::string& GetShaderDirectory();
	static const std::string& GetScenarioDirectory();

	static void Init();

	static std::vector<std::string> GetFilesInDirectory(const std::string&);

private:
	static std::string s_exeDirectory;
	static std::string s_dataDirectory;
	static std::string s_fontsDirectory;
	static std::string s_modelsDirectory;
	static std::string s_shaderDirectory;
	static std::string s_scenarioDirectory;
};