#pragma once
#include <ft2build.h>
#include FT_FREETYPE_H


struct Character
{
	Character();
	Character(GLuint textureId, glm::ivec2 size, glm::ivec2 bearing, GLuint advance);

	GLuint textureId;   // ID handle of the glyph texture
	glm::ivec2 size;    // Size of glyph
	glm::ivec2 bearing; // Offset from baseline to left/top of glyph
	GLuint advance;     // Offset to advance to next glyph
};

class Font
{
public:
	Font();
	void Load(const std::string&, FT_Library&);
	std::map<GLchar, Character> characters;
};