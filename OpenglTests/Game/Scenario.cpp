#include "Scenario.h"


Scenario::Scenario()
	: model("")
	, latitude(0.0f)
	, longitude(0.0f)
	, airspeed(0.0f)
	, roll(0.0f)
	, pitch(0.0f)
	, yaw(0.0f)
	, altitude(0.0f)
	, verticalSpeed(0.0f)
	, second(0.0f)
	, minute(1)
	, hour(1)
	, day(1)
	, month(1)
	, year(1)
{
}

Scenario::Scenario(Scenario&& other)
	: model(std::move(other.model))
	, latitude(std::move(other.latitude))
	, longitude(std::move(other.longitude))
	, airspeed(std::move(other.airspeed))
	, roll(std::move(other.roll))
	, pitch(std::move(other.pitch))
	, yaw(std::move(other.yaw))
	, altitude(std::move(other.altitude))
	, verticalSpeed(std::move(other.verticalSpeed))
	, second(std::move(other.second))
	, minute(std::move(other.minute))
	, hour(std::move(other.hour))
	, day(std::move(other.day))
	, month(std::move(other.month))
	, year(std::move(other.year))
{
}

// Plane state
// ========================
// Base
// --------------
// model
// coords(in lat/lon)
//
// Six pack
// --------------
// airspeed(in knots) - only TAS
// roll
// pitch
// heading/yaw
// altitude
// vertical speed
// 
// World state
// ========================
// time(in zulu/UTC 0)
// date

// Use to index in token list
enum
{
	MODEL,
	LAT, LON,

	TAS,
	ROLL, PITCH, YAW,
	ALT,
	VSPEED,

	SEC, MIN, HOUR,
	DAY, MONTH, YEAR,

	NUM_PROPERTIES
};

std::string Scenario::Serialize()
{
	std::string result;

	result += model + '\n';
	result += std::to_string(latitude) + '\n';
	result += std::to_string(longitude) + '\n';
	result += std::to_string(airspeed) + '\n';
	result += std::to_string(roll) + '\n';
	result += std::to_string(pitch) + '\n';
	result += std::to_string(yaw) + '\n';
	result += std::to_string(altitude) + '\n';
	result += std::to_string(verticalSpeed) + '\n';
	result += std::to_string(second) + '\n';
	result += std::to_string(minute) + '\n';
	result += std::to_string(hour) + '\n';
	result += std::to_string(day) + '\n';
	result += std::to_string(month) + '\n';
	result += std::to_string(year) + '\n';

	return result;
}

void Scenario::LoadFromFile(std::ifstream& file)
{
	std::string line;
	
	std::getline(file, model);
	file >> latitude;
	file >> longitude;
	file >> airspeed;
	file >> roll;
	file >> pitch;
	file >> yaw;
	file >> altitude;
	file >> verticalSpeed;
	file >> second;
	file >> minute;
	file >> hour;
	file >> day;
	file >> month;
	file >> year;
}