#pragma once
#include "IEntity.h"


using FactoryFunction = std::function<IEntity*(uint32)>;


// TODO: Rework to allow whole initialization to be done by the factory
class EntityFactory
{
public:
	static EntityFactory* GetInstance();

	void RegisterFactory(const std::string& entityType, FactoryFunction);
	std::shared_ptr<IEntity> CreateEntity(const std::string& type, uint32 entityId);

private:
	EntityFactory();
	std::map<std::string, FactoryFunction> m_factories;
};

// TODO: Find name
// Use the REGISTER_FACTORY macro
// e.g. REGISTER_FACTORY("fuselage", Fuselage)
class FactoryPlug
{
public:
	FactoryPlug(const std::string&, FactoryFunction);
};

#define REGISTER_FACTORY(type, entity) static FactoryPlug plug(type, [](uint32 id) -> IEntity* { return new entity(id); });