#include "IEntity.h"


template <typename T>
std::weak_ptr<T> IEntity::GetComponent(ComponentType type)
{
	auto it = m_components.find(type);
	if (it != m_components.end())
	{
		StrongComponentPtr pBase(it->second);
		std::shared_ptr<T> pSub(std::static_pointer_cast<T>(pBase));
		return std::weak_ptr<T>(pSub);
	}

	return std::weak_ptr<T>();
}