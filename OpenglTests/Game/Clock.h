#pragma once
#include "Application\Timer.h"
#include "Events\IEventListener.h"


enum Month
{
	JAN, FEB, MAR,
	APR, MAY, JUN,
	JUL, AUG, SEP,
	OCT, NOV, DEC
};


// Controls simulation speed and meta date/time passing.
// I.e. to be used only for the simulation.
class Clock : public IEventListener
{
public:
	Clock();
	float Update();

	float GetDeltaTime() { return m_warpedDelta; }
	float GetCurrentFrame() { return m_timer.currentFrame; }

	float  GetSecond() { return m_second; }
	uint32 GetMinute() { return m_minute; }
	uint32 GetHour() { return m_hour; }
	uint32 GetDay() { return m_day; }
	uint32 GetMonth() { return m_month; }
	uint32 GetYear() { return m_year; }

	void HandleEvent(IEvent*, IEvent::EventType);

private:
	// A value of 1 is supposed to make a nominal simulation
	float m_speedFactor;
	// Meta-game seconds / real-time seconds
	float m_metaPerRealSec;
	float m_warpedDelta;
	float m_elapsed;
	Timer m_timer;

	float  m_second;
	uint32 m_minute;
	uint32 m_hour;
	uint32 m_day;
	uint32 m_month;
	uint32 m_year;
};