#include "EntityFactory.h"


//std::map<std::string, FactoryFunction> EntityFactory::m_factories;

EntityFactory* EntityFactory::GetInstance()
{
	static EntityFactory factory;
	return &factory;
}

EntityFactory::EntityFactory()
{
}

void EntityFactory::RegisterFactory(const std::string& entityType, FactoryFunction factoryFunction)
{
	m_factories[entityType] = factoryFunction;
}

std::shared_ptr<IEntity> EntityFactory::CreateEntity(const std::string& type, uint32 entityId)
{
	IEntity* pEntity = nullptr;

	auto it = m_factories.find(type);
	if (it != m_factories.end())
		pEntity = it->second(entityId);

	if (pEntity)
		return std::shared_ptr<IEntity>(pEntity);
	else
		return nullptr;
}

FactoryPlug::FactoryPlug(const std::string& type, FactoryFunction factoryFunction)
{
	EntityFactory::GetInstance()->RegisterFactory(type, factoryFunction);
}