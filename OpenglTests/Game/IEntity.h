#pragma once
#include "IComponent.h"
#include "World\GeoHelpers.h"


using EntityId = uint32;
using EntityType = uint32;

// TODO: Fix misallignment issue
class IEntity : public std::enable_shared_from_this<IEntity>
{
public:
	virtual ~IEntity();

	void AttachComponent(StrongComponentPtr);

	template <typename T>
	std::weak_ptr<T> GetComponent(ComponentType);

	std::map<ComponentType, StrongComponentPtr> m_components;

	EntityId id;
	bool bDirty;

	glm::mat4 transform;
	glm::vec3 position;
	glm::vec3 scale;
	glm::vec3 orientation; // x -> roll
						   // y -> pitch
						   // z -> yaw

	void SetPosition(const SphericalCoords&);
	void SetPosition(const glm::vec3&);
	void SetHeight(const float);
	void SetOrientation(const glm::vec3&);
	void SetOrientation(const float, const float, const float);
	void Translate(const glm::vec3&);

	virtual void Update(float dt) = 0;

protected:
	IEntity(EntityId);
	void UpdateTransform();
};

#include "IEntity.inl"