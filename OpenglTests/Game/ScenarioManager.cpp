#include "ScenarioManager.h"
#include "Events\EventManager.h"
#include "Resources\FileManager.h"
#include "Scenario.h"
#include "Application\World.h"


ScenarioManager::ScenarioManager(World* pWorld)
	: m_pWorld(pWorld)
	, m_activeScenario(-1)
{
	g_eventManager.RegisterCustomEvent("scenario.load", ConsoleCallback(HandleLoad));
	g_eventManager.RegisterCustomEvent("scenario.save", ConsoleCallback(HandleSave));
}

void ScenarioManager::Init()
{
	m_scenarioFiles = FileManager::GetFilesInDirectory(FileManager::GetScenarioDirectory());
}

void ScenarioManager::HandleLoad(ArgList args)
{
	if (args.size() < 1)
		return;

	args[0] += ".cuc";
	std::ifstream file(FileManager::GetScenarioDirectory() + args[0]);
	
	Scenario scenario;
	try
	{
		scenario.LoadFromFile(file);
		
		m_pWorld->LoadScenario(scenario);
		
		// Change active slot
		m_activeScenario = -1;
		for (uint32 idx = 0; idx < m_scenarioFiles.size(); idx++)
		{
			if (m_scenarioFiles[idx] == args[0])
			{
				m_activeScenario = idx;
				break;
			}
		}
		if (m_activeScenario == -1)
			m_scenarioFiles.push_back(args[0]);
	}
	catch (const std::string& err)
	{
		g_logger.Write(err);
	}
}

void ScenarioManager::HandleSave(ArgList args)
{
	auto serializedState = m_pWorld->MakeScenario().Serialize();
	std::ofstream file;

	if (args.size() == 0)
	{
		// Save on active slot
		if (m_activeScenario == -1)
			return;

		file.open(m_scenarioFiles[m_activeScenario]);
	}
	else
	{
		// Save on new slot
		file.open(args[0]);

		// Make the new slot active
		m_scenarioFiles.push_back(std::move(args[0]));
		m_activeScenario = m_scenarioFiles.size() - 1;
	}

	file << serializedState;
}