#include "Clock.h"


Clock::Clock()
	: m_speedFactor(2.0f)
	, m_metaPerRealSec(1.0f)
	, m_second(1)
	, m_minute(1)
	, m_hour(11)
	, m_day(3)
	, m_month(6)
	, m_year(2015)
	, m_elapsed(0.0f)
	, m_warpedDelta(0.0f)
{
#ifdef WIN32
	SYSTEMTIME localTime;
	GetLocalTime(&localTime);
	
	m_second = localTime.wSecond;
	m_minute = localTime.wMinute;
	m_hour   = localTime.wHour;
	m_day    = localTime.wDay;
	m_month  = localTime.wMonth;
	m_year   = localTime.wYear;
#endif
}

float Clock::Update()
{
	m_timer.Update();

	m_warpedDelta = m_timer.deltaTime * m_speedFactor;

	m_second += m_warpedDelta * m_metaPerRealSec;
	m_minute += m_second / 60;
	m_hour   += m_minute / 60;
	m_day    += m_hour / 24;
	m_month  += m_day / 31; // TODO
	m_year   += m_month / 12;

	m_minute %= 60;
	m_hour   %= 24;
	m_day    %= 31;
	m_month  %= 12;

	if (m_second >= 60.0f)
		m_second = 0.0f;

	m_elapsed += m_warpedDelta;

	return m_warpedDelta;
}

void Clock::HandleEvent(IEvent* pEvent, IEvent::EventType type)
{
}