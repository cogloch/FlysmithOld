#pragma once


// Should rename to something like GameState - maybe
class Scenario
{
public:
	Scenario();
	Scenario(Scenario&&);

	std::string Serialize();
	void LoadFromFile(std::ifstream&);

public:
	// Plane
	std::string model;
	float latitude;
	float longitude;

	float airspeed; // TAS
	float roll;
	float pitch;
	float yaw;
	float altitude;
	float verticalSpeed;

	// World
	float second;
	uint32 minute;
	uint32 hour;
	uint32 day;
	uint32 month;
	uint32 year;
};