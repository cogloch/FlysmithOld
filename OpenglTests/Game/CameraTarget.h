#pragma once
#include "IComponent.h"


struct CameraTarget : public IComponent 
{
	ComponentType GetType() const;
	glm::mat4 GetTransform() const;
};