#pragma once
#include "Game\IEntity.h"
#include "World\GeoHelpers.h"
#include "Game\EntityFactory.h"


class Plane : public IEntity
{
public:
	Plane(EntityId);

	SphericalCoords coords;
	float airspeed;
	//float roll;     | 
	//float pitch;    | bundled with position/
	//float yaw;      | rotation in parent entity class
	//float altitude; |
	float verticalSpeed; // climb rate

	void Update(float dt);

private:
	void InitRenderComponent();
};

REGISTER_FACTORY("plane", Plane)