#include "Plane.h"
#include "Game\RenderComponent.h"
#include "Resources\ResourceManager.h"


Plane::Plane(EntityId id)
	: coords(0.0f, 0.0f)
	, airspeed(0.0f)
	/*, roll(0.0f)
	, pitch(0.0f)
	, yaw(0.0f)
	, altitude(0.0f)*/
	, verticalSpeed(0.0f)
	, IEntity(id)
{
	InitRenderComponent();
}

void Plane::InitRenderComponent()
{
	uint32 shaderProgram = g_resources.GetShaderProgramByName("Model");
	if (!shaderProgram)
		return;

	uint32 resourceId = 0;
	if (resourceId >= g_resources.models.size())
		return;

	auto pComponent = std::make_shared<RenderComponent>(RenderComponent(shaderProgram, resourceId, id));
	AttachComponent(pComponent);
}

void Plane::Update(float dt)
{
	Translate({0.0f, 0.0f, dt * 2.0f});
	g_logger.Write(std::to_string(position.z));
}