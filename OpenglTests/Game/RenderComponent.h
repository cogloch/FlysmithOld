#pragma once
#include "IComponent.h"
#include "Rendering\Renderer.h"


class RenderComponent : public IComponent
{
public:
	RenderComponent(uint32 shader_, uint32 resourceId_, uint32 ownerId_)
		: shader(shader_)
		, resourceId(resourceId_)
		, ownerId(ownerId_)
	{
	}

	ComponentType GetType() const { return ComponentType::RENDER_COMPONENT; }

	uint32 shader;
	uint32 resourceId;
	uint32 ownerId;
};