#include "CameraTarget.h"
#include "IEntity.h"


ComponentType CameraTarget::GetType() const 
{ 
	return ComponentType::CAMERA_TARGET; 
}

glm::mat4 CameraTarget::GetTransform() const 
{
	if (m_pOwner == nullptr)
		return glm::mat4(1.0f);

	auto transform = m_pOwner->transform;
	return transform; 
}