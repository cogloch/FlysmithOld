#pragma once
#include "Events\IEventListener.h"


class World;

// Sort of a save-game that holds the state of the plane and subsequent planned events(to be implemented)
class ScenarioManager //: public IEventListener
{
public:
	ScenarioManager(/*std::shared_ptr<World>*/ World*);
	void Init();
	//void HandleEvent(IEvent*, IEvent::EventType);

private:
	void HandleLoad(ArgList);
	void HandleSave(ArgList);

	//std::shared_ptr<World> m_pWorld;
	World* m_pWorld;
	int m_activeScenario;
	std::vector<std::string> m_scenarioFiles;
};