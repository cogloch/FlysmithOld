#pragma once


enum ComponentType
{
	CAMERA_TARGET,
	RENDER_COMPONENT
};

class IEntity;

class IComponent
{
public:
	virtual ~IComponent() {};
	virtual ComponentType GetType() const = 0;

	//void SetOwner(std::shared_ptr<IEntity> pOwner) { m_pOwner = pOwner; }
	void SetOwner(IEntity* pOwner) { m_pOwner = pOwner; }

protected:
	IComponent() : m_pOwner(nullptr) {}
	//std::shared_ptr<IEntity> m_pOwner;
	IEntity* m_pOwner;
};

using StrongComponentPtr = std::shared_ptr<IComponent>;