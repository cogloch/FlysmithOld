#include "IEntity.h"


IEntity::IEntity(EntityId id_)
	: bDirty(true)
	, transform(1.0f)
	, id(id_)
	, position(0.0f, 0.0f, 0.0f)
	, orientation(0.0f, 0.0f, 0.0f)
	, scale(1.0f, 1.0f, 1.0f)
{
}

IEntity::~IEntity()
{
}

void IEntity::AttachComponent(StrongComponentPtr pComponent)
{
	pComponent->SetOwner(this);

	auto componentType = pComponent->GetType();

	auto it = m_components.find(componentType);
	if (it != m_components.end())
	{
		it->second.reset(pComponent.get());
	}
	else
	{
		m_components[componentType] = pComponent;
	}
}

void IEntity::SetPosition(const SphericalCoords& coords)
{
	position = GeoHelpers::SphericalToCartesian(coords);
	UpdateTransform();
}

void IEntity::SetPosition(const glm::vec3& position_)
{
	position = position_;
	UpdateTransform();
}

void IEntity::SetOrientation(const glm::vec3& orientation_)
{
	orientation = orientation_;
	UpdateTransform();
}

void IEntity::SetOrientation(const float x, const float y, const float z)
{
	orientation = { x, y, z };
	UpdateTransform();
}

void IEntity::SetHeight(const float height)
{
	position.y = height;
	UpdateTransform();
}

void IEntity::Translate(const glm::vec3& vec)
{
	position += vec;
	UpdateTransform();
	//transform = glm::translate(transform, vec);
	//bDirty = true;
}

void IEntity::UpdateTransform()
{
	transform = glm::mat4(1.0f);
	bDirty = true;

	// Translate
	transform = glm::translate(transform, position);
	
	// Rotate
	transform = glm::rotate(transform, orientation.x, glm::vec3(1.0f, 0.0f, 0.0f));
	transform = glm::rotate(transform, orientation.y, glm::vec3(0.0f, 1.0f, 0.0f));
	transform = glm::rotate(transform, orientation.z, glm::vec3(0.0f, 0.0f, 1.0f));
	
	// Scale
	transform = glm::scale(transform, scale);
}