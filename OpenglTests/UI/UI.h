#pragma once
#include "Events\IEventListener.h"
#include "UIElement.h"


// TODO: Move out
// --------------------------------
// Assume child elements are fully contained within the parent
class Node
{
public:
	Node(uint32 id_, UIElement* pElement_ = nullptr) : id(id_), pElement(pElement_) {}
	uint32 id;
	UIElement* pElement;
	std::vector<Node> m_children;
};

class Graph
{
	uint32 nextNodeId;

public:
	Graph() : nextNodeId(1), root(0)
	{
	}

	Node root;

	uint32 AttachNode(UIElement* pElement, uint32 parent = 0)
	{
		std::stack<Node*> searchFront;
		searchFront.push(&root);
		while (!searchFront.empty())
		{
			Node* pCurrent = searchFront.top();
			searchFront.pop();

			if (pCurrent->id == parent)
			{
				Node newNode(nextNodeId, pElement);
				pCurrent->m_children.push_back(newNode);
				break;
			}

			for (auto& pConsidered : pCurrent->m_children)
				searchFront.push(&pConsidered);
		}

		nextNodeId++;
		return nextNodeId - 1;
	}
};
// --------------------------------

class UI : public IEventListener
{
public:
	UI();
	void Render();
	void HandleEvent(IEvent*, IEvent::EventType);

private:
	void OnWindowResize(const glm::ivec2&);
	void OnWindowResize(float, float);

	Graph m_graph;
};