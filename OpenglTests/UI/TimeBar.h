#pragma once
#include "Rendering\Quad.h"
#include "UIElement.h"


class TimeBar : public UIElement
{
public:
	TimeBar();
	void Render();
	Quad m_background;
};