#pragma once
#include "Events\IEventListener.h"
#include "Rendering\Quad.h"
#include "Rendering\RenderProxy.h"
#include "Application\Timer.h"
#include "UIElement.h"
#include "Utils\Tree.h"


struct ConsoleRenderProxy
{
	SpriteRenderProxy blinker;
};

class Console : public UIElement
{
public:
	Console();
	void Init();
	void HandleEvent(IEvent*, IEvent::EventType);
	bool IsVisible();

	void Render();

	void Write(const std::string&);

private:
	void OnWindowResize(uint32, uint32);
	void HandlePrint(ArgList);

	void UpdateSuggestions();
	std::vector<std::string> m_suggestions;
	TreeNode<std::function<void(ArgList)>>* m_pCommandTree;

	bool m_bVisible;
	bool m_bInputActive;

	Timer m_timer;
	Quad  m_bgHistory;
	Quad  m_bgSuggestions;
	Quad  m_blinker;
	float m_bottomCoordY;

	std::string				 m_line;
	std::vector<std::string> m_history;
};

extern Console g_console;