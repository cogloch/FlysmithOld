#pragma once
#include "Events\IEventListener.h"


class UIElement : public IEventListener
{
public:
	virtual ~UIElement();
	virtual void HandleEvent(IEvent*, IEvent::EventType);
	virtual void Render() = 0;
	
protected:
	UIElement();
	virtual void OnWindowResize(uint32, uint32);

	// Value in the range 0.0-1.0 indicating the element's position/size relative to the screen
	// i.e. if the position == 0.7 and the screen is 800 units wide, the element will be positioned at
	// 0.7 * 800 = 560
	glm::vec2 m_size;
	glm::vec2 m_position;
};