#include "UIElement.h"
#include "Events\WindowEv.h"


UIElement::UIElement()
{
	RegisterForEvent(IEvent::WINDOW_RESIZE);
}

UIElement::~UIElement()
{
}

void UIElement::HandleEvent(IEvent* pEvent, IEvent::EventType type)
{
	if (type == IEvent::WINDOW_RESIZE)
	{
		auto newSize = dynamic_cast<WindowResizeEv*>(pEvent)->newSize;
		OnWindowResize(newSize.x, newSize.y);
	}
}

void UIElement::OnWindowResize(uint32, uint32)
{
}