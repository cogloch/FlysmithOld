#include "UI.h"
#include "Events\WindowEv.h"
#include "Application\EngineConfig.h"
#include "Rendering\SpriteRenderer.h"
#include "Console.h"


UI::UI()
{
	RegisterForEvent(IEvent::WINDOW_RESIZE);
	OnWindowResize(g_config.windowSize);

	m_graph.AttachNode(&g_console);
}

void UI::HandleEvent(IEvent* pEvent, IEvent::EventType evType)
{
	switch (evType)
	{
	case IEvent::WINDOW_RESIZE:
		OnWindowResize(dynamic_cast<WindowResizeEv*>(pEvent)->newSize);
		break;
	}
}

void UI::OnWindowResize(const glm::ivec2& newSize)
{
	OnWindowResize(static_cast<float>(newSize.x), static_cast<float>(newSize.y));
}

void UI::OnWindowResize(float width, float height)
{
	SpriteRenderer::SetProjection(glm::ortho(0.0f, width, height, 0.0f, -1.0f, 1.0f));
}

void UI::Render()
{
	std::queue<Node*> renderQueue;
	for (auto& pRenderNode : m_graph.root.m_children)
		renderQueue.push(&pRenderNode);

	while (!renderQueue.empty())
	{
		auto pCurrent = renderQueue.front();
		renderQueue.pop();
		pCurrent->pElement->Render();
		for (auto& pRenderNode : pCurrent->m_children)
			renderQueue.push(&pRenderNode);
	}
}