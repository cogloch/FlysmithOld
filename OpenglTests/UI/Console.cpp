#include "Console.h"
#include "Events\KeyboardEv.h"
#include "Resources\ResourceManager.h"
#include "Application\InputManager.h"
#include "Events\MouseEv.h"
#include "Events\WindowEv.h"
#include "Events\EventManager.h"
#include "Application\EngineConfig.h"
#include "Rendering\Text.h"
#include "Rendering\SpriteRenderer.h"


Console::Console() 
	: m_bVisible(false)
	, m_bInputActive(false)
{
	m_pCommandTree = &g_eventManager.m_commandTree;

	m_blinker.SetSize(1.7f, 12.0f);
	m_blinker.SetColor({ 1.0f, 1.0f, 1.0f, 1.0f });

	m_bgSuggestions.SetColor({ 1.0f / 61.0f, 1.0f / 43.0f, 1.0f / 31.0f, 0.9f });
	m_bgHistory.SetColor({ 1.0f / 61.0f, 1.0f / 43.0f, 1.0f / 31.0f, 0.8f });
}

void Console::Init()
{
	RegisterForEvent(IEvent::KEY_UP);
	RegisterForEvent(IEvent::KEY_CHAR);
	RegisterForEvent(IEvent::KEY_REPEAT);
	RegisterForEvent(IEvent::MOUSE_LBUTTON_DOWN);
	RegisterForEvent(IEvent::MOUSE_RBUTTON_DOWN);
	RegisterForEvent(IEvent::WINDOW_RESIZE);

	OnWindowResize(g_config.windowSize.x, g_config.windowSize.y);

	RegisterCustomEvent("print", ConsoleCallback(HandlePrint));
}

void Console::OnWindowResize(uint32 width_, uint32 height_)
{
	float width = static_cast<float>(width_);
	float height = static_cast<float>(height_);

	m_bottomCoordY = 0.3f * height;
	m_bgHistory.SetSize(width * 0.7f, m_bottomCoordY);
	m_bgSuggestions.SetSize(width * 0.3f, m_bottomCoordY);
	m_bgSuggestions.SetPosition({ width * 0.7f, 0.0f });
}

void Console::Write(const std::string& msg)
{
	m_history.push_back(msg);
}

void Console::HandlePrint(ArgList args)
{
	if (args.size() < 1)
		return;

	Write(args[0]);
}

void Console::HandleEvent(IEvent* pEvent, IEvent::EventType evType)
{
	switch (evType)
	{
	case IEvent::WINDOW_RESIZE:
	{
		auto newSize = dynamic_cast<WindowResizeEv*>(pEvent)->newSize;
		OnWindowResize(newSize.x, newSize.y);
		break;
	}
	case IEvent::KEY_UP:
	{
		auto key = dynamic_cast<KeyboardEv*>(pEvent)->keyCode;

		if (key == GLFW_KEY_GRAVE_ACCENT)
		{
			// Input may be inactive while the console is visible i.e. they are not necessarily equal at one time
			m_bInputActive = !m_bVisible;
			m_bVisible = !m_bVisible;
			UpdateSuggestions();
		}
		else if (m_bInputActive)
		{
			if (!m_line.size())
				break;

			if (key == GLFW_KEY_ENTER)
			{
				m_history.push_back(m_line);
				g_eventManager.PostCustomEvent(m_line);
				m_line = "";
				UpdateSuggestions();
			}

			if (key == GLFW_KEY_BACKSPACE)
			{
				m_line.pop_back();
				if (!m_line.size())
					break;

				//if (m_line[m_line.size() - 1] == '.')
				UpdateSuggestions();
			}
		}
		break;
	}
	case IEvent::KEY_REPEAT:
	{
		if (m_line.size())
			m_line.pop_back();
		UpdateSuggestions();
		break;
	}
	case IEvent::KEY_CHAR:
	{
		auto key = dynamic_cast<KeyboardEv*>(pEvent)->keyCode;
		if (m_bInputActive && (key != GLFW_KEY_GRAVE_ACCENT))
		{
			m_line += key;
			if (key == GLFW_KEY_PERIOD)
				UpdateSuggestions();
		}
		break;
	}
	case IEvent::MOUSE_RBUTTON_DOWN:
	case IEvent::MOUSE_LBUTTON_DOWN:
	{
		auto mouseEv = dynamic_cast<MouseEv*>(pEvent);
		if (mouseEv->y > m_bottomCoordY)
			m_bInputActive = false;
		if (mouseEv->y < m_bottomCoordY)
			m_bInputActive = true;
		break;
	}
	}
}

bool Console::IsVisible()
{
	return m_bVisible;
}

void Console::UpdateSuggestions()
{
	m_suggestions.clear();

	if (!m_line.size())
	{
		for (const auto& child : m_pCommandTree->children)
			m_suggestions.push_back(child.sKey);
		return;
	}

	std::string tempLine = "root." + m_line;

	std::vector<std::string> tokens;
	std::size_t startPos = 0;
	bool remaining = true;
	while (remaining)
	{
		std::size_t foundPos = tempLine.find('.', startPos);
		
		if (foundPos == std::string::npos)
		{
			//tokens.push_back(tempLine.substr(startPos));
			break;
		}
		else
		{
			tokens.push_back(tempLine.substr(startPos, foundPos - startPos));
		}
		
		startPos = foundPos + 1;
	}

	for (std::vector<std::string>::const_reverse_iterator it = tokens.rbegin(), end = tokens.rend(); it != end; ++it)
	{
		auto foundTree = m_pCommandTree->FindChild(*it);
		if (foundTree)
		{
			for (const auto& child : foundTree->children)
				m_suggestions.push_back(child.sKey);
			return;
		}
	}
}

void Console::Render()
{
	static const float spacing = 18.0f;
	m_timer.Update();

	if (m_bVisible)
	{
		// Background
		SpriteRenderer::Render(m_bgHistory);
		SpriteRenderer::Render(m_bgSuggestions);
		
		float yPosition = m_bottomCoordY - 10.0f;
		
		// Current line
		TextRenderer::Render("> " + m_line, 1, { 20.0f, yPosition }, 0.3f, { 1.0f, 1.0f, 1.0f });
		yPosition -= spacing;

		// History
		for (std::vector<std::string>::const_reverse_iterator it = m_history.rbegin(), end = m_history.rend(); it != end; ++it)
		{
			TextRenderer::Render("> " + *it, 1, { 20.0f, yPosition }, 0.3f, { 1.0f, 1.0f, 1.0f });
			yPosition -= spacing;
		}

		// Suggestions
		yPosition = 20.0f;
		for (const auto& suggestion : m_suggestions)
		{
			TextRenderer::Render(suggestion, 1, { static_cast<float>(g_config.windowSize.x) * 0.72, yPosition }, 0.3f, { 1.0f, 1.0f, 1.0f });
			yPosition += spacing;
		}

		// Blinker
		if (m_bInputActive && (sin(5 * m_timer.currentFrame) > 0))
		{
			m_blinker.SetPosition(30.0f + (m_line.size() + 0.55f) * 8.0f,						  
							      m_bottomCoordY - spacing);
			SpriteRenderer::Render(m_blinker);
		}
	}
}