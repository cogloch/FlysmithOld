#include "Timer.h"


Timer::Timer() : currentFrame(0.0f), lastFrame(0.0f), deltaTime(0.0f) 
{
	Update();
}

void Timer::Update()
{
	currentFrame = (float)glfwGetTime();
	deltaTime = currentFrame - lastFrame;
	lastFrame = currentFrame;
}