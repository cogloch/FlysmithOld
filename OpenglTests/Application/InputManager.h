#pragma once
#include "Events\IEventListener.h"


enum MouseButton
{
	MOUSE_LEFT,
	MOUSE_RIGHT
};

// Keeps track of input state, not events
class InputManager : public IEventListener
{
public:
	InputManager();

	void HandleEvent(IEvent*, IEvent::EventType);
	bool IsKeyDown(int key);
	bool IsMouseBtnDown(MouseButton);

private:
	void HandleMouseMove(float x, float y);

	// Keyboard
	bool m_keys[1024];

	// Mouse
	float m_lastMousePosX;
	float m_lastMousePosY;
	bool m_mouseBtn[2];
	bool m_firstMouse;
};

extern InputManager g_inputManager;