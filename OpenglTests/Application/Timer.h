#pragma once


class Timer
{
public:
	Timer();
	void Update();

	float currentFrame;
	float lastFrame;
	float deltaTime;
};