#pragma once
#include "Events\EventManager.h"


class HardwareCaps
{
public:
	void Init();
	// videoModes[monitorIndex][videoModeIndex]
	// monitorIndex = 0 is the primary monitor
	std::vector<std::vector<glm::ivec2>> videoModes;

private:
	void HandleListVideoModes(ArgList);
};

extern HardwareCaps g_hwCaps;