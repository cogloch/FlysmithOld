#pragma once
#include "Events\IEventListener.h"


class EngineConfig : public IEventListener
{
public:
	EngineConfig();
	void Init(GLFWwindow*);
	void HandleEvent(IEvent*, IEvent::EventType);

	glm::ivec2 windowSize;

private:
	GLFWwindow* m_pWindow;
};

extern EngineConfig g_config;