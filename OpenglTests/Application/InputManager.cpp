#include "InputManager.h"
#include "Events\KeyboardEv.h"
#include "Events\MouseEv.h"
#include "Events\EventManager.h"


InputManager::InputManager() : m_firstMouse(true), m_lastMousePosX(400.0f), m_lastMousePosY(300.0f)
{
	RegisterForEvent(IEvent::MOUSE_RBUTTON_DOWN);
	RegisterForEvent(IEvent::MOUSE_RBUTTON_UP);
	RegisterForEvent(IEvent::KEY_UP);
	RegisterForEvent(IEvent::KEY_DOWN);

	for (auto& key : m_keys) key = false;
	for (auto& btn : m_mouseBtn) btn = false;
}

void InputManager::HandleEvent(IEvent* pEvent, IEvent::EventType evType)
{
	switch (evType)
	{
	case IEvent::KEY_UP:
	{
		m_keys[dynamic_cast<KeyUpEv*>(pEvent)->keyCode] = false;
		break;
	}
	case IEvent::KEY_DOWN:
	{
		m_keys[dynamic_cast<KeyDownEv*>(pEvent)->keyCode] = true;
		break;
	}
	case IEvent::MOUSE_RBUTTON_DOWN:
	{
		m_mouseBtn[MOUSE_RIGHT] = true;
		break;
	}
	case IEvent::MOUSE_RBUTTON_UP:
	{
		m_mouseBtn[MOUSE_RIGHT] = false;
		break;
	}
	case IEvent::MOUSE_MOVE:
	{
		auto mouseMoveEv = dynamic_cast<MouseMoveEv*>(pEvent);
		HandleMouseMove(mouseMoveEv->x, mouseMoveEv->y);
		delete pEvent; // Any other event than MouseMove gets directly to the EventManager, which handles
					   // their deletion. MouseMove is special like that.
	}
	}
}

bool InputManager::IsKeyDown(int key)
{
	return m_keys[key];
}

bool InputManager::IsMouseBtnDown(MouseButton button)
{
	return m_mouseBtn[button];
}

void InputManager::HandleMouseMove(float x, float y)
{
	if (m_firstMouse)
	{
		m_lastMousePosX = x;
		m_lastMousePosY = y;
		m_firstMouse = false;
	}

	g_eventManager.PostEvent(new MouseMoveEv(x - m_lastMousePosX, m_lastMousePosY - y));

	m_lastMousePosX = x;
	m_lastMousePosY = y;
}