#include "Window.h"
#include "HardwareCaps.h"
#include "Events\WindowEv.h"
#include "Events\EventManager.h"


Window::Window() :
	m_size(800, 600),
	m_title("Kebab"),
	m_bFullscreen(false),
	m_pDisplayMonitor(nullptr),
	m_pVideoMode(nullptr),
	m_pWindow(nullptr)
{
}

Window::Window(glm::ivec2 size, const std::string& title, bool bFullscreen) :
	m_size(size),
	m_title(title),
	m_bFullscreen(bFullscreen),
	m_pDisplayMonitor(nullptr),
	m_pVideoMode(nullptr),
	m_pWindow(nullptr)
{
}

void Window::SetFullscreen(bool bFullscreen)
{
	if (m_bFullscreen == bFullscreen)
		return;

	if (bFullscreen)
		m_pDisplayMonitor = glfwGetPrimaryMonitor();
	else
		m_pDisplayMonitor = nullptr;

	m_bFullscreen = bFullscreen;

	RecreateWindow();
}

bool Window::IsFullscreen() const
{
	return m_bFullscreen;
}

void Window::SetSize(glm::ivec2 size)
{
	if (m_size == size)
		return;
	m_size = size;

	g_eventManager.PostEvent(new WindowResizeEv(m_size));

	glfwSetWindowSize(m_pWindow, size.x, size.y);
}

glm::ivec2 Window::GetSize() const
{
	return m_size;
}

void Window::SetTitle(const std::string& title)
{
	if (title == m_title)
		return;
	m_title = title;

	glfwSetWindowTitle(m_pWindow, title.c_str());
}

const std::string& Window::GetTitle() const
{
	return m_title;
}

GLFWwindow* Window::GetWindow() const
{
	return m_pWindow;
}

GLFWwindow* Window::Init()
{
	if (m_bFullscreen)
		m_pDisplayMonitor = glfwGetPrimaryMonitor();

	EnumerateVideoModes();

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_DECORATED, GL_FALSE);

	glfwWindowHint(GLFW_SAMPLES, 4);

	RecreateWindow();
	
	glfwMakeContextCurrent(m_pWindow);
	glEnable(GL_MULTISAMPLE);
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	return m_pWindow;
}

void Window::EnumerateVideoModes()
{
	int numMonitors = 0;
	GLFWmonitor** ppMonitors = glfwGetMonitors(&numMonitors);
	g_hwCaps.videoModes.resize(numMonitors);

	for (int monitorIdx = 0; monitorIdx < numMonitors; monitorIdx++)
	{
		int numVideoModes = 0;
		auto pVideoModes = glfwGetVideoModes(*(ppMonitors + monitorIdx), &numVideoModes);
		g_hwCaps.videoModes[monitorIdx].reserve(numVideoModes);

		for (int modeIdx = 0; modeIdx < numVideoModes; modeIdx++)
		{
			if ((pVideoModes + modeIdx)->width < 800)
				continue;

			g_hwCaps.videoModes[monitorIdx].push_back({ 
				(pVideoModes + modeIdx)->width, 
				(pVideoModes + modeIdx)->height 
			});
		}
	}
}

void Window::RecreateWindow()
{
	int32 maxSize = 0,
		  maxWidth = 0,
		  maxHeight = 0;

	// Create the biggest possible framebuffer, then downscale as needed
	for (auto& monitor:g_hwCaps.videoModes)
		for (auto& resolution : monitor)
		{
			int32 size = resolution.x * resolution.y;
			if (size > maxSize)
			{
				maxSize = size;
				maxWidth = resolution.x;
				maxHeight = resolution.y;
			}
		}

	m_pWindow = glfwCreateWindow(maxWidth, maxHeight, m_title.c_str(), m_pDisplayMonitor, nullptr);
	glfwSetWindowPos(m_pWindow, 0, 0);
}