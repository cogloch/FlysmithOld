#include "World.h"
#include "Events\KeyboardEv.h"
#include "InputManager.h"
#include "Resources\ResourceManager.h"
#include "Game\Plane\Plane.h"
#include "Game\RenderComponent.h"
#include "Game\CameraTarget.h"
#include "Game\EntityFactory.h"


World::World() 
	: m_scenarioManager(this)
	, m_bInitialized(false)
{
	RegisterCustomEvent("world.createEntity", ConsoleCallback(HandleCreateEntity));
}

void World::Init()
{
	if (m_bInitialized)
		return;

#if PROF_INIT_WORLD
	Timer timer;
#endif

	m_cityManager.Init();
	m_scenarioManager.Init();
	
#if PROF_INIT_WORLD
	timer.Update();
	g_logger.Write("World initialization time: " + std::to_string(timer.deltaTime));
#endif

	m_bInitialized = true;
}

void World::LoadScenario(const Scenario& scenario)
{
	entities.erase(entities.begin(), entities.end());

	auto plane = std::dynamic_pointer_cast<Plane>(
		EntityFactory::GetInstance()->CreateEntity("plane", entities.size())
		);

	plane->SetPosition({ scenario.latitude, scenario.longitude });
	plane->SetHeight(scenario.altitude);
	plane->SetOrientation(scenario.roll, scenario.pitch, scenario.yaw);

	plane->airspeed = scenario.airspeed;
	plane->verticalSpeed = scenario.verticalSpeed;
	
	AddEntity(plane);
}

Scenario World::MakeScenario()
{
	Scenario scenario;
	return scenario;
}

void World::HandleCreateEntity(ArgList args)
{
	if (args.size() < 1)
		return;

	auto pEntity = EntityFactory::GetInstance()->CreateEntity(args[0], entities.size());
	AddEntity(pEntity);
}

void World::Update(float dt)
{
	// 1. Update plane 
	// 2. Update city manager
	
	for (auto& entity : entities)
		entity->Update(dt);
}

void World::HandleEvent(IEvent* pEvent, IEvent::EventType evType)
{
}

bool World::HasEntity(EntityId id)
{
	return (entities.size() > id);
}

uint32 World::AddEntity(std::shared_ptr<IEntity> pEntity)
{
	entities.push_back(pEntity);
	return (entities.size() - 1);
}

void World::RemoveEntity(uint32 id)
{
	// TODO: Remove entity
	for (auto it = entities.begin(), end = entities.end(); it != end; it++)
	{
	}
}