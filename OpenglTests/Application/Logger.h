#pragma once

#define LogWriteScoped(msg) g_logger.Write(std::string(__FILE__) + ": " + std::string(__func__) + ", " + std::to_string(__LINE__) + ": " + msg);

class Logger
{
public:
	Logger();
	~Logger();

	void Init();
	void Close();

	void Write(const std::string&, bool writeSeparator = true);
	void WriteSeparator();

private:
	std::string	  m_buffer;
	std::ofstream m_logStream;
};

extern Logger g_logger;