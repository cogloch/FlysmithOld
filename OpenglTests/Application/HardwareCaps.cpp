#include "HardwareCaps.h"
#include "UI\Console.h"


void HardwareCaps::Init()
{
	g_eventManager.RegisterCustomEvent("hw.listVideoModes", ConsoleCallback(HandleListVideoModes));
}

void HardwareCaps::HandleListVideoModes(ArgList)
{
	for (uint32 monitorIdx = 0; monitorIdx < videoModes.size(); monitorIdx++)
	{
		g_console.Write("Monitor " + std::to_string(monitorIdx) + ":");
		g_console.Write("-------------------");
		
		for (auto& resolution : videoModes[monitorIdx])
			g_console.Write("Width: " + std::to_string(resolution[0]) + " Height: " + std::to_string(resolution[1]));
	}
}