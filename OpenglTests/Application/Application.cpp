#include "Application.h"
#include "Resources\ResourceManager.h"
#include "Resources\FileManager.h"
#include "Events\EventManager.h"
#include "Events\KeyboardEv.h"
#include "Events\MouseEv.h"
#include "Events\TickEv.h"
#include "HardwareCaps.h"
#include "InputManager.h"
#include "Logger.h"
#include "UI\Console.h"
#include "Game\RenderComponent.h"


// Force nvidia optimus to use the dedicated gpu
extern "C" {
	_declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
}

ResourceManager g_resources;
EventManager g_eventManager;
InputManager g_inputManager;
Logger		 g_logger;
EngineConfig g_config;
HardwareCaps g_hwCaps;
Console		 g_console;

Application::Application() 
	: m_renderer(&m_world)
{
}

Application::~Application() {}

void OnGlfwError(int error, const char* description)
{
	g_logger.Write("GLFW Error: " + std::string(description));
}

void Application::Init()
{
	FileManager::Init();
	g_logger.Init();

	glfwInit();
	glfwSetErrorCallback(OnGlfwError);

	m_window.Init();
	g_config.Init(m_window.GetWindow());

	g_hwCaps.Init();

	glewExperimental = GL_TRUE;
	glewInit();

	g_resources.Init();

	m_renderer.Init(m_window.GetWindow());

	g_console.Init();

	m_world.Init();
}

void Application::Terminate()
{
	glfwTerminate();
}

void HandleMouseMoveEvent(GLFWwindow* pWindow, double posX, double posY)
{
	// The MouseMove event is supposed to hold the movement offset. 
	// The input manager handles that and sends forth another MouseMove 
	// event with the offset to the EventManager.
	g_inputManager.HandleEvent(new MouseMoveEv(static_cast<float>(posX), static_cast<float>(posY)), IEvent::MOUSE_MOVE);
}

void HandleMouseScrollEvent(GLFWwindow* pWindow, double offsetX, double offsetY)
{
	g_eventManager.PostEvent(new MouseScrollEv(static_cast<float>(offsetX), static_cast<float>(offsetY)));
}

void HandleMousePressEvent(GLFWwindow* pWindow, int button, int action, int mods)
{
	double dPosX, dPosY;
	glfwGetCursorPos(pWindow, &dPosX, &dPosY);

	float posX = static_cast<float>(dPosX);
	float posY = static_cast<float>(dPosY);

	switch (button)
	{
	case GLFW_MOUSE_BUTTON_RIGHT:
		if (action == GLFW_PRESS)
			g_eventManager.PostEvent(new RMouseDownEv(posX, posY));
		else if (action == GLFW_RELEASE)
			g_eventManager.PostEvent(new RMouseUpEv(posX, posY));
	case GLFW_MOUSE_BUTTON_LEFT:
		if (action == GLFW_PRESS)
			g_eventManager.PostEvent(new LMouseDownEv(posX, posY));
		else if (action == GLFW_RELEASE)
			g_eventManager.PostEvent(new LMouseUpEv(posX, posY));
	}
}

void HandleKeyEvent(GLFWwindow* pWindow, int key, int scancode, int action, int mode)
{
	// Laptop function keys(probably)
	if (key < 0)
		return;

	if (action == GLFW_PRESS)
	{
		g_eventManager.PostEvent(new KeyDownEv(key));
	}
	else if (action == GLFW_RELEASE)
	{
		g_eventManager.PostEvent(new KeyUpEv(key));

		if (key == GLFW_KEY_ESCAPE)
			glfwSetWindowShouldClose(pWindow, GL_TRUE);
	}
	else if (action == GLFW_REPEAT)
	{
		if (key == GLFW_KEY_BACKSPACE)
			g_eventManager.PostEvent(new KeyRepeatEv(key));
	}
}

void HandleCharEvent(GLFWwindow* pWindow, uint32 codepoint, int mods)
{
	g_eventManager.PostEvent(new KeyCharEv(codepoint));
}

void Application::MainLoop()
{
	GLFWwindow* pWindow = m_window.GetWindow();

	glfwSetCharModsCallback(pWindow, HandleCharEvent);
	glfwSetKeyCallback(pWindow, HandleKeyEvent);
	glfwSetCursorPosCallback(pWindow, HandleMouseMoveEvent);
	glfwSetMouseButtonCallback(pWindow, HandleMousePressEvent);
	glfwSetScrollCallback(pWindow, HandleMouseScrollEvent);

	while (!glfwWindowShouldClose(pWindow))
	{
		glfwPollEvents();

		g_eventManager.PostEvent(new TickEv());
		g_eventManager.DispatchEvents();

		m_world.Update(m_clock.Update());

#if DISABLE_RENDERER_MT
		UpdateRenderData();
		m_renderer.STRender();
#else
		// TODO: Signal the other way around
		std::lock_guard<std::mutex> toUpdate(m_renderer.semaphore.mtx);

		if (!m_renderer.semaphore.dataReady)
		{
			//std::lock_guard<std::mutex>(m_renderer.semaphore.mtx);
			UpdateRenderData();
			m_renderer.semaphore.dataReady = true;
			m_renderer.semaphore.condVariable.notify_one();
		}
#endif
	}
}

uint32 numVertsDisplayed = 0;

void Application::UpdateRenderData()
{
	if (numVertsDisplayed != m_renderer.m_scene.tempNumVertStat)
	{
		numVertsDisplayed = m_renderer.m_scene.tempNumVertStat;
		glfwSetWindowTitle(m_window.GetWindow(), ("Kebab - " + std::to_string(numVertsDisplayed) + " vertices").c_str());
	}

	// Currently match entities with their graphical counterpart using like indices.
	
	uint32 toAdd = m_world.entities.size() - m_renderer.m_scene.drawables.size();
	while (toAdd--)
		m_renderer.m_scene.drawables.push_back({});

	for (uint32 idx = 0; idx < m_world.entities.size(); idx++)
		if (m_world.entities[idx]->bDirty)
		{
			m_world.entities[idx]->bDirty = false;

			auto pComponent = m_world.entities[idx]->GetComponent<RenderComponent>(RENDER_COMPONENT).lock();
			m_renderer.m_scene.drawables[idx].resourceId = pComponent->resourceId;
			m_renderer.m_scene.drawables[idx].shader = pComponent->shader;
			m_renderer.m_scene.drawables[idx].transform = m_world.entities[idx]->transform;
		}
}
