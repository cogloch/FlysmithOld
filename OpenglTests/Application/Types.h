#pragma once


#ifdef _WIN32
using int16 = __int16;
using int32 = __int32;
using int64 = __int64;

using uint16 = unsigned __int16;
using uint32 = unsigned __int32;
using uint64 = unsigned __int64;
#endif