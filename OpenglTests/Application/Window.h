#pragma once


class Window 
{
public:
	Window();
	Window(glm::ivec2 size, const std::string& title, bool bFullscreen);

	void SetFullscreen(bool bFullscreen);
	bool IsFullscreen() const;

	void SetSize(glm::ivec2 size);
	glm::ivec2 GetSize() const;

	void SetTitle(const std::string& title);
	const std::string& GetTitle() const;

	GLFWwindow* GetWindow() const;

	GLFWwindow* Init();

private:
	void EnumerateVideoModes();
	void RecreateWindow();

	GLFWwindow*  m_pWindow;
	GLFWvidmode* m_pVideoMode;
	GLFWmonitor* m_pDisplayMonitor;
	
	bool		m_bFullscreen;
	glm::ivec2  m_size;
	std::string m_title;
};