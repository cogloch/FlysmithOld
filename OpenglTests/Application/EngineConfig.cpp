#include "EngineConfig.h"
#include "Events\WindowEv.h"
#include "HardwareCaps.h"


EngineConfig::EngineConfig()
{
	RegisterForEvent(IEvent::MOUSE_RBUTTON_DOWN);
	RegisterForEvent(IEvent::MOUSE_RBUTTON_UP);
	RegisterForEvent(IEvent::WINDOW_RESIZE);
}

void EngineConfig::Init(GLFWwindow* pWindow)
{
	m_pWindow = pWindow;

	// TODO: Read config from file 
	windowSize = g_hwCaps.videoModes[0][g_hwCaps.videoModes[0].size() - 1];
}

void EngineConfig::HandleEvent(IEvent* pEvent, IEvent::EventType evType)
{
	switch (evType)
	{
	case IEvent::MOUSE_RBUTTON_DOWN:
		glfwSetInputMode(m_pWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		break;
	case IEvent::MOUSE_RBUTTON_UP:
		glfwSetInputMode(m_pWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		break;
	case IEvent::WINDOW_RESIZE:
		windowSize = dynamic_cast<WindowResizeEv*>(pEvent)->newSize;
		break;
	}
}