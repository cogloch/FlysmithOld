#pragma once
#include "Window.h"
#include "EngineConfig.h"
#include "Rendering\Renderer.h"
#include "World.h"
#include "Game\Clock.h"


class Application
{
public:
	Application();
	~Application();

	void Init();
	void Terminate();
	void MainLoop();

private:
	void UpdateRenderData();

	Clock	 m_clock;
	Window   m_window;
	World    m_world;
	Renderer m_renderer;
};