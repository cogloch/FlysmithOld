#pragma once
#include "Game\IEntity.h"
#include "Events\IEventListener.h"
#include "World\CityManager.h"
#include "Game\ScenarioManager.h"
#include "Game\Scenario.h"


class World : public IEventListener, std::enable_shared_from_this<World>
{
public:
	World();
	
	void Init();
	void LoadScenario(const Scenario&);
	Scenario MakeScenario();

	void Update(float deltaTime);
	void HandleEvent(IEvent*, IEvent::EventType);

	// Returns the entity's assigned id
	// The actual creation of the entity is handled by its factory
	uint32 AddEntity(std::shared_ptr<IEntity>);

	void RemoveEntity(uint32 id);

	std::vector<std::shared_ptr<IEntity>> entities;
	bool HasEntity(EntityId);

private:
	bool m_bInitialized;
	void HandleCreateEntity(ArgList);
	std::stack<uint32> m_entitySlots;
	CityManager m_cityManager;
	ScenarioManager m_scenarioManager;
	Scenario m_scenario;
};

using WorldPtr = std::shared_ptr<World>;