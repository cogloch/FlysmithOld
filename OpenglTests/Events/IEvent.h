#pragma once


class IEvent
{
public:
	enum EventType
	{
		// Mouse input
		MOUSE_LBUTTON_UP,
		MOUSE_LBUTTON_DOWN,
		MOUSE_RBUTTON_UP,
		MOUSE_RBUTTON_DOWN,
		MOUSE_MOVE,
		MOUSE_SCROLL,

		// Keyboard input
		KEY_UP,
		KEY_DOWN,
		KEY_CHAR,
		KEY_REPEAT,

		// FPSCamera
		CAMERA_MOVE,
		CAMERA_ENABLE,
		CAMERA_DISABLE,
		CAMERA_FOCUS_CHANGE,

		// Game session
		GAME_START,
		GAME_OVER,

		// Network events
		NET_JOIN,

		TICK,

		WINDOW_RESIZE,

		ENTITY_CREATE,

		NUM_EVENTS
	};

	virtual EventType GetType() = 0;
};