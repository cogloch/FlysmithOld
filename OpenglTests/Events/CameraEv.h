#pragma once
#include "IEvent.h"
#include "Game\CameraTarget.h"


class CameraFocusChangeEv : public IEvent
{
public:
	CameraFocusChangeEv(std::shared_ptr<CameraTarget> pNewTarget_) : pNewTarget(pNewTarget_) {}
	IEvent::EventType GetType() { return IEvent::CAMERA_FOCUS_CHANGE; }
	std::shared_ptr<CameraTarget> pNewTarget;
};