#include "EventManager.h"


EventManager::EventManager() {}

EventManager::~EventManager()
{
	Close();
}

void EventManager::Close()
{
	// TODO: Lock main queue and clear it too.
	for (auto& pEvent : m_eventQueues[1])
		if (pEvent)
			delete pEvent;

	m_eventQueues[1].clear();

	//for (auto& queue : m_eventQueues)
	//	if (queue.size())
	//	{
	//		for (auto& pEvent : queue)
	//		{
	//			g_logger.Write("Deleting " + std::to_string(pEvent->GetType()));
	//			if (pEvent)
	//				delete pEvent;
	//		}

	//		queue.clear();
	//	}
}

void EventManager::PostCustomEvent(const std::string& command)
{
	// First token is the command, followed by arguments
	std::stringstream ss(command);
	std::istream_iterator<std::string> begin(ss);
	std::istream_iterator<std::string> end;
	ArgList tokens(begin, end);

	std::vector<std::string> path;
	std::size_t startPos = 0;

	bool remaining = true;
	while (remaining)
	{
		std::size_t foundPos = tokens[0].find('.', startPos);
		if (foundPos == std::string::npos)
		{
			remaining = false;
			path.push_back(tokens[0].substr(startPos));
		}
		else
		{
			path.push_back(tokens[0].substr(startPos, foundPos - startPos));
			startPos = foundPos + 1;
		}
	}

	auto currentTree = m_commandTree.FindChild(path[path.size() - 1]);
	if (!currentTree)
		return;
	
	for (auto it = path.begin() + 1, end = path.end(); it != end; it++)
	{
		currentTree = currentTree->FindChild(*it);
		if (!currentTree)
			return;
	}

	if (currentTree)
	{
		ArgList args(tokens.begin() + 1, tokens.end());
		currentTree->data(args);
	}
}

void EventManager::RegisterCustomEvent(const std::string& commandTag, std::function<void(ArgList)> callback)
{
	std::vector<std::string> path;
	std::size_t startPos = 0;

	bool remaining = true;
	while (remaining)
	{
		std::size_t foundPos = commandTag.find('.', startPos);
		if (foundPos == std::string::npos)
		{
			remaining = false;
			path.push_back(commandTag.substr(startPos));
		}
		else
		{
			path.push_back(commandTag.substr(startPos, foundPos - startPos));
			startPos = foundPos + 1;
		}
	}

	auto currentTree = m_commandTree.FindChild(path[0]);
	if (!currentTree)
		currentTree = m_commandTree.AddChild(path[0], [](ArgList) { g_logger.Write("Attempted call to command category, and not command."); });

	for (uint32 idx = 1; idx < path.size(); idx++)
	{
		auto subTree = currentTree->FindChild(path[idx]);
		if (!subTree)
			subTree = currentTree->AddChild(path[idx], 
											[](ArgList) { g_logger.Write("Attempted call to command category, and not command."); },
										    path[idx - 1]);

		currentTree = subTree;
	}

	currentTree->data = callback;

	auto MATA = m_commandTree.FindChild(path[path.size() - 1]);
	MATA->data;
}

void EventManager::RegisterListener(IEvent::EventType eventType, IEventListener* pListener)
{
	m_eventListeners[eventType].push_back(pListener);
}

void EventManager::RemoveListener(IEventListener* pListener)
{
	for (auto& eventType : m_eventListeners)
		for (unsigned int idx = 0; idx < eventType.size(); idx++)
			if (pListener == eventType[idx])
				eventType.erase(eventType.begin() + idx);
}

void EventManager::PostEvent(IEvent* pEvent)
{
	m_eventQueues[SECOND_Q].push_back(pEvent);
}

void EventManager::TriggerEvent(IEvent* pEvent)
{
	IEvent::EventType type = pEvent->GetType();
	for (auto& pListener : m_eventListeners[type])
		pListener->HandleEvent(pEvent, type);

	delete pEvent;
}

void EventManager::DispatchEvents()
{
	m_eventQueues[MAIN_Q].clear();
	m_eventQueues[MAIN_Q] = std::move(m_eventQueues[SECOND_Q]);

	for (auto& pEvent : m_eventQueues[MAIN_Q])
	{
		IEvent::EventType type = pEvent->GetType();
		for (auto& pListener : m_eventListeners[type])
			pListener->HandleEvent(pEvent, type);

		delete pEvent;
	}
}