#include "MouseEv.h"

#define MOUSE_EV_CONSTRUCTOR(event) (event::event) (float x_, float y_) : MouseEv(x_, y_) {}
#define MOUSE_EV_GET_TYPE(event, type) IEvent::EventType (event::GetType()) { return (type); }


MouseEv::MouseEv(float x_, float y_) : x(x_), y(y_) {}
MOUSE_EV_CONSTRUCTOR(LMouseUpEv)
MOUSE_EV_CONSTRUCTOR(LMouseDownEv)
MOUSE_EV_CONSTRUCTOR(RMouseUpEv)
MOUSE_EV_CONSTRUCTOR(RMouseDownEv)
MOUSE_EV_CONSTRUCTOR(MouseMoveEv)
MOUSE_EV_CONSTRUCTOR(MouseScrollEv)

MOUSE_EV_GET_TYPE(LMouseUpEv, IEvent::MOUSE_LBUTTON_UP)
MOUSE_EV_GET_TYPE(LMouseDownEv, IEvent::MOUSE_LBUTTON_DOWN)
MOUSE_EV_GET_TYPE(RMouseUpEv, IEvent::MOUSE_RBUTTON_UP)
MOUSE_EV_GET_TYPE(RMouseDownEv, IEvent::MOUSE_RBUTTON_DOWN)
MOUSE_EV_GET_TYPE(MouseMoveEv, IEvent::MOUSE_MOVE)
MOUSE_EV_GET_TYPE(MouseScrollEv, IEvent::MOUSE_SCROLL)