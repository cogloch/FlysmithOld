#pragma once
#include "IEvent.h"


class WindowResizeEv : public IEvent
{
public:
	WindowResizeEv(glm::ivec2);
	EventType GetType();
	glm::ivec2 newSize;
};