#pragma once
#include "IEvent.h"

using ArgList = std::vector<std::string>;
#define ConsoleCallback(Func) [=](ArgList args) { this->Func(args); }


class IEventListener
{
public:
	virtual ~IEventListener();
	virtual void HandleEvent(IEvent*, IEvent::EventType) = 0;

protected:
	void RegisterForEvent(IEvent::EventType);

	// Pass a member function wrapped in ConsoleCallback():
	// RegisterCustomEvent("doStuff", ConsoleCallback(DoStuffHandler));
	void RegisterCustomEvent(const std::string& command, std::function<void(ArgList)>);
};