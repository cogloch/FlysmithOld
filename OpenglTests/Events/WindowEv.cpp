#include "WindowEv.h"


WindowResizeEv::WindowResizeEv(glm::ivec2 size) : newSize(size) {}

IEvent::EventType WindowResizeEv::GetType()
{
	return IEvent::WINDOW_RESIZE;
}