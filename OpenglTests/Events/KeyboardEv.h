#pragma once
#include "IEvent.h"


class KeyboardEv : public IEvent
{
public:
	int keyCode;

protected:
	KeyboardEv(int);
};

class KeyUpEv : public KeyboardEv
{
public:
	KeyUpEv(int);
	EventType GetType();
};

class KeyDownEv : public KeyboardEv
{
public:
	KeyDownEv(int);
	EventType GetType();
};

class KeyCharEv : public KeyboardEv
{
public:
	KeyCharEv(int);
	EventType GetType();
};

class KeyRepeatEv : public KeyboardEv
{
public:
	KeyRepeatEv(int);
	EventType GetType();
};