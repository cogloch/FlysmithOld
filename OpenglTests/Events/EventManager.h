#pragma once
#include "IEvent.h"
#include "IEventListener.h"
#include "Utils\Tree.h"


using ArgList = std::vector<std::string>;
#define ConsoleCallback(Func) [=](ArgList args) { this->Func(args); }


class EventManager
{
public:
	EventManager();
	~EventManager();

	void Close();

	void RegisterListener(IEvent::EventType, IEventListener*);
	void RemoveListener(IEventListener*);

	void PostEvent(IEvent*);
	void TriggerEvent(IEvent*);

	void DispatchEvents();

	void RegisterCustomEvent(const std::string& command, std::function<void(ArgList)>);
	void PostCustomEvent(const std::string&);

private:
	enum EventQueueChannel
	{
		MAIN_Q,
		SECOND_Q,
	
		NUM_CHANNELS
	};

	friend class Console;
	TreeNode<std::function<void(ArgList)>> m_commandTree;

	std::vector<IEvent*> m_eventQueues[NUM_CHANNELS];
	std::vector<IEventListener*> m_eventListeners[IEvent::NUM_EVENTS];
};

extern EventManager g_eventManager;