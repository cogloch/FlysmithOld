#pragma once
#include "Events\IEvent.h"


class TickEv : public IEvent
{
public:
	TickEv();
	EventType GetType();
};