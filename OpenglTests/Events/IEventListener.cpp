#include "IEventListener.h"
#include "EventManager.h"


IEventListener::~IEventListener()
{
	g_eventManager.RemoveListener(this);
}

void IEventListener::RegisterForEvent(IEvent::EventType type)
{
	g_eventManager.RegisterListener(type, this);
}

void IEventListener::RegisterCustomEvent(const std::string& command, std::function<void(ArgList)> callback)
{
	g_eventManager.RegisterCustomEvent(command, callback);
}