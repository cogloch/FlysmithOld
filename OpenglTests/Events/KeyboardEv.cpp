#include "KeyboardEv.h"

#define KEY_EV_CONSTRUCTOR(event) (event::event) (int key) : KeyboardEv(key) {}
#define KEY_EV_GET_TYPE(event, type) IEvent::EventType (event::GetType()) { return (type); }


KeyboardEv::KeyboardEv(int key) : keyCode(key) {}
KEY_EV_CONSTRUCTOR(KeyUpEv)
KEY_EV_CONSTRUCTOR(KeyDownEv)
KEY_EV_CONSTRUCTOR(KeyCharEv)
KEY_EV_CONSTRUCTOR(KeyRepeatEv)

KEY_EV_GET_TYPE(KeyUpEv, IEvent::KEY_UP)
KEY_EV_GET_TYPE(KeyDownEv, IEvent::KEY_DOWN)
KEY_EV_GET_TYPE(KeyCharEv, IEvent::KEY_CHAR)
KEY_EV_GET_TYPE(KeyRepeatEv, IEvent::KEY_REPEAT)