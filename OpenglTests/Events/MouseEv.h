#pragma once
#include "IEvent.h"


class MouseEv : public IEvent
{
public:
	float x;
	float y;

protected:
	MouseEv(float, float);
};

class LMouseUpEv : public MouseEv
{
public:
	LMouseUpEv(float, float);
	EventType GetType();
};

class LMouseDownEv : public MouseEv
{
public:
	LMouseDownEv(float, float);
	EventType GetType();
};

class RMouseUpEv : public MouseEv
{
public:
	RMouseUpEv(float, float);
	EventType GetType();
};

class RMouseDownEv : public MouseEv
{
public:
	RMouseDownEv(float, float);
	EventType GetType();
};

class MouseMoveEv : public MouseEv
{
public:
	MouseMoveEv(float, float);
	EventType GetType();
};

class MouseScrollEv : public MouseEv
{
public:
	MouseScrollEv(float, float);
	EventType GetType();
};