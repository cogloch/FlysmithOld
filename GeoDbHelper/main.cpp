#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>


struct City
{
	City() {}
	City(City&& other)
		: name(std::move(other.name))
		, population(std::move(other.population))
		, latitude(std::move(other.latitude))
		, longitude(std::move(other.longitude))
	{
	}

	std::wstring name;
	std::wstring population;
	std::wstring latitude;
	std::wstring longitude;

	static int nextId;
};

int City::nextId = 0;

// lat long
std::vector<int> world[180][360];

std::wostream& operator <<(std::wostream& stream, const City& city)
{
	stream << city.name << " " << city.population << " " << city.latitude << " " << city.longitude;
	return stream;
}

void WriteData(std::wofstream& file, const std::wstring& country, std::vector<City>& cities)
{
	// Bundle cities by country
	file << '~' << country << '\n';

	/*std::sort(cities.begin(), cities.end(), [](City a, City b){
		return a.region < b.region;
	});*/

	//std::wstring currentRegion = L"Cucuolia";
	for (const auto& city : cities)
	{
		// Bundle cities by region
		/*if (currentRegion != city.region)
		{
			currentRegion = city.region;
			file << '+' << currentRegion << '\n';
		}*/
		
		file << city << '\n';
	}

	cities.clear();
}

bool MakeCity(const std::wstring& line, City& city, std::wstring& country)
{
	enum
	{
		COUNTRY,
		ANSI_CITY,
		CITY,
		REGION,
		POPULATION,
		LAT,
		LONG
	};

	// Remove ASCII-encoded city names from final record
	// Country | ANSI-City | City | Region | Population | Lat | Long 
	std::vector<std::wstring> tokens;

	bool bTokensRemaining = false;
	size_t lastSepPosition = 0;

	do
	{
		bTokensRemaining = false;

		size_t sepPosition = line.find(',', lastSepPosition);
		if (sepPosition != std::string::npos)
			bTokensRemaining = true;

		tokens.push_back(line.substr(lastSepPosition, sepPosition - lastSepPosition));

		lastSepPosition = sepPosition + 1;
	} while (bTokensRemaining);

	// Discard records with no population data
	if (!tokens[POPULATION].size())
		return false;

	country = tokens[COUNTRY];
	city.name = tokens[CITY];
	//city.region = tokens[REGION];
	city.population = tokens[POPULATION];
	city.latitude = tokens[LAT];
	city.longitude = tokens[LONG];

	City::nextId++;

	// std::stoi can't accept arguments < 1 in the form: .123 for 0.123
	float fLat = std::stof(tokens[LAT]);
	float fLon = std::stof(tokens[LONG]);

	// Shift latitude by 90 and longitude by 180
	int lat = static_cast<int>(fLat + 90);
	int lon = static_cast<int>(fLon + 180);
	
	world[lat][lon].push_back(City::nextId);

	return true;
}

void ClusterLatLong()
{
	std::ofstream file("clusteredCities.txt");
	
	for (int lat = 0; lat < 180; lat++)
	{
		for (int lon = 0; lon < 360; lon++)
		{
			file << "# ";
			for (const auto& cityId : world[lat][lon])
				file << cityId << ' ';
			file << '\n';
		}
	}
}

int main()
{
	std::wifstream fileIn("worldcitiespop.txt");
	if (fileIn.fail())
		return -1;

	std::wofstream fileOut("cityData.txt");

	std::wstring line;

	// First line contains column description
	std::getline(fileIn, line);

	std::vector<City> cities;
	std::wstring currentCountry = L"";
	while (std::getline(fileIn, line))
	{
		City city;
		std::wstring country;
		if (MakeCity(line, city, country))
		{
			//Bundle cities by country
			if (country != currentCountry)
			{
				WriteData(fileOut, currentCountry, cities);
				currentCountry = country;
			}
			cities.push_back(std::move(city));
		}
	}

	ClusterLatLong();
}